/*
Name:				app_gui_gfx.h
Library:			LCD Driver for STM32 MCUs
Written by:			Ronald Bazillion
Date written:		02/13/2020
Description:		This is the lcd implementation file for the sharp LS018B7DH02

Resources:

Modifications:
    02/13/2020      Ronald Bazillion        First Draft

-----------------
*/

#ifndef __LCD_IMAGE_FCNS_H_
#define __LCD_IMAGE_FCNS_H_

//***** Header files *****//
#include "stm32l4xx_hal.h"
#include <stdbool.h>

//*******Defines***************//


//***** typedefs enums *****//


//*******Flags****************//


//***** Constant variables and typedefs *****//


//*******Structures****************//


//***** Global Variables ************//


//***** API Functions prototype *****//

void APP_GUI_Location(void);
void APP_GUI_AlanGraphic(void);
void APP_GUI_ShaunGraphic(void);


#endif
