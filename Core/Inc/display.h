/*
 * Copyright (c) 2019 Lynq Technologies
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * is NOT PERMITTED without express written consent of Lynq Technologies.
 */


/*
 * Display.h
 *
 *  Created on: Nov 25, 2019
 *      Author: NKP
 */
#ifndef DISPLAY_H
#define DISPLAY_H

#include "fast_math.h"
#include "lynq_config.h"

#ifdef __cplusplus
extern "C" {
#endif

#define WHITE 1
#define BLACK 0

class Display: public FastMathClass
{
 public:

  typedef struct
  {
    uint16_t size;
    uint16_t color;
    uint16_t backgroundcolor;
    bool wrap;
  } Font_t;

  typedef enum
  {
    SILOM6,
    SILOM12,

  } CustomFont_t;

  typedef struct
  {
    int16_t x;
    int16_t y;

  } Position_t;

  typedef struct
  {
    uint16_t width;
    uint16_t height;

  } Dimension_t;

  typedef Dimension_t Resolution_t;

  typedef enum
  {
    SHAPE_Point,
    SHAPE_Line,
    SHAPE_Circle,
    SHAPE_Triangle,
    SHAPE_Rectangle,
    SHAPE_FillRect,
    SHAPE_FillRoundRect,
    SHAPE_Arc,
    SHAPE_Arrow,
    SHAPE_BoldArc

  } Shape_t;

  typedef Position_t ShapePoint_t;

  typedef struct
  {
    Position_t start;
    Position_t end;

  } ShapeLine_t;

  typedef struct
  {
    Position_t center;
    uint16_t radius;

  } ShapeCircle_t;

  typedef struct
  {
    Position_t first;
    Position_t second;
    Position_t third;

  } ShapeTriangle_t;

  typedef struct
  {
    Position_t start;
    uint16_t width;
    uint16_t height;

  } ShapeRectangle_t;

  typedef struct
  {
    uint16_t radius;
    uint16_t heading;
    uint16_t arcLength;
    uint16_t x;
    uint16_t y;

  } ShapeArc_t;

  typedef uint16_t ShapeArrow_t;

  typedef struct
  {
    uint8_t hours;
    uint8_t mins;
    uint8_t seconds;

  } Time_t;

  Display();
  void initDisplay(void);

  void drawLynqLabel(uint16_t color);
  void drawShape(Shape_t shape, uint16_t color, void * argp);
  void drawArc(uint16_t angle,uint16_t arcLength);
  void drawMainScreen(char *namePtr, uint16_t nameLen, Time_t* time);
  void drawMemberScreen(char *namePtr, uint16_t nameLen, uint16_t angle,uint16_t arcLength, uint16_t distance);
  void drawTime(Time_t* time);
  void drawNameWithTag(char *namePtr, uint16_t nameLen);

//  void setDistance(uint16_t distance);
  void setDistance(uint16_t distance, uint16_t cursorOffsetX = 0, uint16_t cursorOffsetY = 0);
//  void setName(char *namePtr, uint16_t nameLen);
  void setName(char *namePtr, uint16_t nameLen, uint16_t cursorOffsetX = 0, uint16_t cursorOffsetY = 0);
  void setCursorOffset(uint16_t x, uint16_t y);
  void setLineNum(uint8_t lineNum);
  void setDisplayPtr(uint16_t index, uint8_t data);

  uint8_t* getDisplayPtr(uint16_t index);

  void centerTextCust(char *text, uint16_t nameLen, uint8_t lineNum);
//  void customTextposition(char *text, uint16_t nameLen, uint8_t lineNum);
  void customTextposition(char *text, uint16_t nameLen, uint16_t cursorOffsetX, uint16_t cursorOffsetY);

  size_t printcrlf(void);
  size_t println(char* s, uint16_t nameLen);
  size_t print(char* s, uint16_t nameLen);
  size_t writeStream(const uint8_t *buffer, size_t size);

  void testTrigFunct();
  void testShapes();
  void testCharacters();

 private:

  Resolution_t resolution;
  bool arrowSmoothing;
  bool backLightOn;
  uint8_t fontWidth, fontHeight;
  uint8_t m_lineNum;
  uint16_t m_cursorOffsetX;
  uint16_t m_cursorOffsetY;


};

extern Display g_Display;

#ifdef __cplusplus
}
#endif

#endif
