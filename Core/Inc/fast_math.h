/*
 * Copyright (c) 2019 Lynq Technologies
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * is NOT PERMITTED without express written consent of Lynq Technologies.
 */

/*
 * FastMath.h
 *
 *  Created on: Nov 25, 2019
 *      Author: NKP
 */

#ifndef FASTMATH_H
#define FASTMATH_H

#include <stdint.h>
#include <math.h>
#include "lynq_config.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Used to calculate the trigonometric function from a lookUp table */
class FastMathClass
{

 public:
  float isin(long x);/* Calcluates sin(Theta) */
  float icos(long x);/* Calcluates cos(Theta) */
  float itan(long x);/* Calcluates tan(Theta) */
  float fsin(float d);
  float fcos(float d);
  float atantwo(float y, float x);
  double FABS(float f);

 private:
  const uint8_t iSinTable8[SIN_TABLE_8_ARRAY_SIZE]=
  {
    0,   4,   9,   13,  18,  22,  27,  31,  35,  40,  44,
    49,  53,  57,  62,  66,  70,  75,  79,  83,  87,
    91,  96,  100, 104, 108, 112, 116, 120, 124, 128,

    131, 135, 139, 143, 146, 150, 153, 157, 160, 164,
    167, 171, 174, 177, 180, 183, 186, 190, 192, 195,
    198, 201, 204, 206, 209, 211, 214, 216, 219, 221,

    223, 225, 227, 229, 231, 233, 235, 236, 238, 240,
    241, 243, 244, 245,	246, 247, 248, 249, 250, 251,
    252, 253, 253, 254, 254, 254, 255, 255, 255, 255
  };
};

#ifdef __cplusplus
}
#endif

#endif
