/*
 * Copyright (c) 2019 Lynq Technologies
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * is NOT PERMITTED without express written consent of Lynq Technologies.
 */

/*
 * Graphics.h
 *
 *  Created on: Nov 25, 2019
 *      Author: NKP
 */
#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "lynq_config.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _swap_int16_t
#define _swap_int16_t(a, b) { int16_t t = a; a = b; b = t; }
#endif
#ifndef _swap_uint16_t
#define _swap_uint16_t(a, b) { uint16_t t = a; a = b; b = t; }
#endif

#ifndef pgm_read_byte
#define pgm_read_byte(addr) (*(const unsigned char *)(addr))
#endif
#ifndef pgm_read_word
#define pgm_read_word(addr) (*(const unsigned short *)(addr))
#endif
#ifndef pgm_read_dword
#define pgm_read_dword(addr) (*(const unsigned long *)(addr))
#endif

class Graphics
{
 public:
  typedef struct	// Data stored PER GLYPH
  {
    uint16_t bitmapOffset;     // Pointer into GFX_FONT->bitmap
    uint8_t  width, height;    // Bitmap dimensions in pixels
    uint8_t  xAdvance;         // Distance to advance cursor (x axis)
    int8_t   xOffset, yOffset; // Dist from cursor pos to UL corner
  } GFX_GLYPH;

  typedef struct 	// Data stored for FONT AS A WHOLE:
  {
    uint8_t  *bitmap;      // Glyph bitmaps, concatenated
    GFX_GLYPH *glyph;       // Glyph array
    uint8_t   first, last; // ASCII extents
    uint8_t   yAdvance;    // Newline distance (y axis)
  } GFX_FONT;

  //Init. functions
  void initGraphics();

  //Get functions
  uint8_t getFontXAdvance(void);
  uint8_t getFontYAdvance(void);
  int16_t getCursorX(void);
  int16_t getCursorY(void);
  uint16_t getStringLength(char * str);
  uint16_t getColor(uint16_t color);
  int16_t width(void);
  int16_t height(void);
  bool getInvertDisplay(void);
  void getTextBounds(char *str, int16_t x, int16_t y,int16_t *x1, int16_t *y1, uint16_t *w, uint16_t *h);
  uint8_t* getDisplayPtr(uint16_t index);

  //Set functions
  void setFont(const GFX_FONT *f);
  void setCursor(int16_t x, int16_t y);
  void setTextSize(uint8_t s);
  void setTextColor(uint16_t c);
  void setTextColorWithBG(uint16_t c, uint16_t b);
  void setTextWrap(bool w);
  void setBuffer(void);
  void setInvertDisplay(bool i);
  void setDisplayPtr(uint16_t index, uint8_t data);

  //Draw Functions
  void drawCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color);
  void drawCircleHelper( int16_t x0, int16_t y0, int16_t r, uint8_t cornername, uint16_t color);
  void drawRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
  void drawRoundRect(int16_t x, int16_t y, int16_t w, int16_t h, int16_t r, uint16_t color);
  void drawTriangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color);
  void drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color);
  void drawFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color);
  void drawFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color);
  void drawBitmapImage(int16_t x, int16_t y, const uint8_t *bitmap, int16_t w, int16_t h, uint16_t color);
  void drawChar(int16_t x, int16_t y, unsigned char c, uint16_t color, uint16_t bg, uint8_t size);
  void drawPixel(int16_t x, int16_t y, uint16_t color);
  void drawBitmap(int16_t x, int16_t y, const uint8_t *bitmap, int16_t w, int16_t h, uint16_t color);

  //Fill functions
  void fillCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color);
  void fillCircleHelper(int16_t x0, int16_t y0, int16_t r, uint8_t cornername, int16_t delta, uint16_t color);
  void fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
  void fillRoundRect(int16_t x, int16_t y, int16_t w, int16_t h, int16_t r, uint16_t color);
  void fillTriangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color);
  void fillScreen(uint16_t color);

  //Clear functions
  void clearRect(void);
  void clearBuffer(void);

  //Helper functions
//  void cp437(bool x);
  void write(uint8_t c);

 private:

  uint8_t m_displayBuffer[BUFFER_SIZE];
  // const int16_t WIDTH, HEIGHT;
  int16_t m_widthSize;
  int16_t m_heightSize;
  int16_t m_cursorX;
  int16_t m_cursorY;

  uint16_t m_textColor;
  uint16_t m_textBgColor;

  uint8_t m_textSize;

  bool m_wrap;
  bool m_invertDisplayFlag;
  GFX_FONT *m_gfxFontPtr;
};

#ifdef __cplusplus
}
#endif

extern Graphics g_Graphics;

#endif
