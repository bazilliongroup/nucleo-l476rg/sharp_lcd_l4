/*
Name:				lcd_graphics_prototypes.h
Library:			LCD Driver for STM32 MCUs
Written by:			Ronald Bazillion
Date written:		02/13/2020
Description:		graphics prototypes for Adafruit graphics

Resources:

Modifications:
    02/13/2020      Ronald Bazillion        First Draft

-----------------
*/

#ifndef __LCD_GRAPHICS_PROTOTYPES_H_
#define __LCD_GRAPHICS_PROTOTYPES_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

//**********************graphics Library prototypes********************************************************************

//defined in display_wrapper.cpp *************DISPLAY_WRAPPER*******************
//
void LynqGraphics_initDisplayLibrary(void);
void LynqGraphics_drawDisplayLynqLabel(uint16_t color);
void LynqGraphics_drawShapeLine(uint16_t color, int16_t xStart, int16_t yStart, int16_t xEnd, int16_t yEnd);
void LynqGraphics_drawShapePoint(uint16_t color, int16_t x, int16_t y);
void LynqGraphics_drawShapeCircle(uint16_t color, int16_t x, int16_t y, uint16_t radius);
void LynqGraphics_drawShapeTriangle(uint16_t color, int16_t firstX, int16_t firstY,
		   int16_t secondX, int16_t secondY,
		   int16_t thirdX, int16_t thirdY);
void LynqGraphics_drawShapeRectangle(uint16_t color, int16_t x, int16_t y, int16_t width, int16_t height);
void LynqGraphics_drawShapeFillRect(uint16_t color, int16_t x, int16_t y, int16_t width, int16_t height);
void LynqGraphics_drawShapeFillRoundRect(uint16_t color, int16_t x, int16_t y, int16_t width, int16_t height);
void LynqGraphics_drawShapeArc(uint16_t color, int16_t radius, int16_t heading, int16_t arcLength);
void LynqGraphics_drawShapeArrow(uint16_t color, int16_t arrowHead);
void LynqGraphics_drawShapeBoldArc(uint16_t color, int16_t x, int16_t y, int16_t radius, int16_t heading, int16_t arcLength);
void LynqGraphics_drawNameWithTag(char *namePtr, uint16_t nameLen);
void LynqGraphics_drawMemberScreen(char *namePtr, uint16_t nameLenIn, uint16_t angleIn, uint16_t arcLengthIn, uint16_t distance);
void LynqGraphics_drawMainScreen(char *namePtr, uint16_t nameLen, uint8_t hours, uint8_t min, uint8_t sec);
void LynqGraphics_drawArc(uint16_t angle, uint16_t arcLength);
void LynqGraphics_drawTime(uint8_t hours, uint8_t mins, uint8_t seconds);

size_t LynqGraphics_writeStream(const uint8_t *buffer, size_t size);
size_t LynqGraphics_print(char *s, uint16_t nameLen);
size_t LynqGraphics_println(char *s, uint16_t nameLen);
size_t LynqGraphics_printcrlf(void);

void LynqGraphics_centerTextCust(char *text, uint16_t nameLen, uint8_t lineNum);
void LynqGraphics_customTextposition(char *text, uint16_t nameLen, uint16_t x, uint16_t y);
void LynqGraphics_setName(char *namePtr, uint16_t nameLen);
void LynqGraphics_setCoordName(char *namePtr, uint16_t nameLen, uint16_t cursorOffsetX, uint16_t cursorOffsetY); //Overloaded setName in Display.cpp
void LynqGraphics_setDistance(uint16_t distance);
void LynqGraphics_setCoordDistance(uint16_t distance, uint16_t cursorOffsetX, uint16_t cursorOffsetY); //Overloaded setDistance in Display.cpp
void LynqGraphics_setCursorOffset(uint16_t x, uint16_t y);
void LynqGraphics_setLineNum(uint8_t lineNum);
void LynqGraphics_setDisplayPtr(uint16_t index, uint8_t data);

uint8_t* LynqGraphics_getDisplayPtr(uint16_t index);

//defined in graphic_wrapper.cpp *************GRAPHICS_WRAPPER*******************
//
void Graphics_clearRect(void);
void Graphics_clearBuffer(void);

void Graphics_setInvertDisplay(bool index);
void Graphics_setBuffer(void);
void Graphics_setCursor(int16_t x, int16_t y);
void Graphics_setTextSize(uint8_t s);
void Graphics_setTextColor(uint16_t c);
void Graphics_setTextColorWithBG(uint16_t c, uint16_t b);
void Graphics_setTextWrap(bool w);
void Graphics_setFont(uint8_t *bitmap, uint8_t first, uint8_t last, uint8_t yAdvance, 	//GFX_FONT - Data stored for FONT AS A WHOLE: (a GFX_GLYPH* is in the GFX_FONT
		              uint16_t bitmapOffset, uint8_t  width, uint8_t  height, uint8_t xAdvance, int8_t   xOffset, int8_t   yOffset);	//GFX_GLYPH - Data stored PER GLYPH
void Graphics_setDisplayPtr(uint16_t index, uint8_t data);

uint8_t Graphics_getFontXAdvance(void);
uint8_t Graphics_getFontYAdvance(void);
uint8_t Graphics_getFontXAdvance(void);
uint16_t Graphics_getStringLength(char * str);
uint16_t Graphics_getColor(uint16_t color);
int16_t Graphics_getCursorX(void);
int16_t Graphics_getCursorY(void);
int16_t Graphics_getWidth(void);
int16_t Graphics_getHeight(void);
bool Graphics_getInvertDisplay(void);

void Graphics_drawBitmap(int16_t x, int16_t y, const uint8_t *bitmap, int16_t w, int16_t h, uint16_t color);
void Graphics_drawPixel(int16_t x, int16_t y, uint16_t color);
void Graphics_drawChar(int16_t x, int16_t y, unsigned char c, uint16_t color, uint16_t bg, uint8_t size);
void Graphics_drawCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color);
void Graphics_drawCircleHelper( int16_t x0, int16_t y0, int16_t r, uint8_t cornername, uint16_t color);
void Graphics_drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color);
void Graphics_drawRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
void Graphics_drawFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color);
void Graphics_drawFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color);
void Graphics_drawRoundRect(int16_t x, int16_t y, int16_t w, int16_t h, int16_t r, uint16_t color);
void Graphics_drawTriangle(int16_t x0, int16_t y0,int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color);
void Graphics_drawBitmapImage(int16_t x, int16_t y, const uint8_t *bitmap, int16_t w, int16_t h, uint16_t color);

void Graphics_fillCircle(int16_t x0, int16_t y0, int16_t r,uint16_t color);
void Graphics_fillCircleHelper(int16_t x0, int16_t y0, int16_t r, uint8_t cornername, int16_t delta, uint16_t color);
void Graphics_fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
void Graphics_fillScreen(uint16_t color);
void Graphics_fillRoundRect(int16_t x, int16_t y, int16_t w, int16_t h, int16_t r, uint16_t color);
void Graphics_fillTriangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color);

void Graphics_write(uint8_t c);

#endif

