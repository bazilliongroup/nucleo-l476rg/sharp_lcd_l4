/*
Name:				sharp_lcd.h
Library:			LCD Driver for STM32 MCUs
Written by:			Ronald Bazillion
Date written:		02/13/2020
Description:		This is the lcd implementation file for the sharp LS018B7DH02

Resources:

Modifications:
    02/13/2020      Ronald Bazillion        First Draft

-----------------
*/

#ifndef __LCD_SHARP_H_
#define __LCD_SHARP_H_

//***** Header files *****//
#include "stm32l4xx_hal.h"
#include <stdbool.h>
//*******Defines***************//
#define WHITE 1
#define BLACK 2
#define SINGULAR 1
#define MULTIPLE 2

#define LCD_WR 		0x80
#define LCD_WR_BR   0x01
#define LCD_VCOM 	0x40
#define LCD_VCOM_BR	0x02
#define LCD_CLR		0x20
#define LCD_CLR_BR	0x04

#define NUM_OF_MODE_BYTES                   01
#define NUM_OF_LINE_ADDRESS_BYTES           01
#define NUM_OF_SING_TRAILER_BYTES           02
#define NUM_OF_COMMAND_TRAILER_BYTES        01

#define LS018B7DH02 1
#define LS011B7DH03 2
#define LCD_DEVICE LS018B7DH02

#if LCD_DEVICE == LS011B7DH03
#define LS011B7DH03_HEIGHT 160
#define LS011B7DH03_WIDTH  068
#define NUM_OF_DATA_BYTES (LS011B7DH03_HEIGHT / 8)	// 20 bytes
#elif LCD_DEVICE == LS018B7DH02
#define DISPLAY_PIXEL_SIZE   230
#define DISPLAY_LENGTH_SIZE  303
#define DISPLAY_DUMMY_BYTES   10
#define NUM_OF_DATA_BYTES ((DISPLAY_PIXEL_SIZE + DISPLAY_DUMMY_BYTES) / 8)  //30 bytes
#endif

#define NUM_OF_PIXEL_BYTES      (NUM_OF_MODE_BYTES + NUM_OF_LINE_ADDRESS_BYTES + NUM_OF_DATA_BYTES + NUM_OF_SING_TRAILER_BYTES) //34 bytes
#define NUM_OF_COMMAND_BYTES    (NUM_OF_MODE_BYTES + NUM_OF_COMMAND_TRAILER_BYTES) //2 BYTES

//***** typedefs enums *****//

//*******Flags****************//

//***** Constant variables and typedefs *****//

//*******Structures****************//

typedef struct _lcd_sharp_config
{
	GPIO_TypeDef* gpioCsPort;
	uint16_t gpioCsPin;
	GPIO_TypeDef* gpioDispPort;
	uint16_t gpioDispPin;
	GPIO_TypeDef* gpioExtComInPort;
	uint16_t gpioExtComInPin;
	GPIO_TypeDef* gpioExtModePort;
	uint16_t gpioExtModePin;
	SPI_HandleTypeDef hspi;
	bool extMode;

}LCD_SHARP_CONFIG;

//***** Global Variables ************//


//***** API Functions prototype *****//


void LCD_Sharp_Init(LCD_SHARP_CONFIG* lcdConfig);
void LCD_Sharp_InitScreen(int color);
void LCD_Sharp_ClearScreen(void);
void LCD_Sharp_Invalidate(uint16_t startLength, uint16_t endLength, uint16_t startPixel, uint16_t endPixel, bool clearSel);
void LCD_Sharp_DisplayImage(uint16_t edWidth, uint16_t edLen, int color, const uint8_t* inGraphic);
void LCD_Sharp_WritePartialUpdate(uint16_t startLength,	uint16_t endLength, uint16_t startPixel, 	uint16_t endPixel, const uint8_t* inGraphic);
void LCD_Sharp_PowerOffSequence(void);
void LCD_Sharp_SendVcom(void);
void LCD_Sharp_SendData(void);
void LCD_Sharp_MvGraphicsDisplayToBuffer(void);
void LCD_Sharp_GraphicsTest(void);
void LCD_Sharp_InterruptCallBack_ISR(uint8_t var);


#endif
