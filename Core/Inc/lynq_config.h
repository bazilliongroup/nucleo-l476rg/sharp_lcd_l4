/*
 * Copyright (c) 2019 Lynq Technologies
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * is NOT PERMITTED without express written consent of Lynq Technologies.
 */

/*
 * Config.h
 *
 *  Created on: Nov 25, 2019
 *      Author: NKP
 */

#ifndef CONFIG_H
#define	CONFIG_H

// #define DEBUGLOW

#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <String.h>

#ifdef __cplusplus
extern "C" {
#endif

#define SCREENDIMENSIONSWIDTH 240		//Horizontal dimention
#define SCREENDIMENSIONSHEIGHT 303		//Vertical dimention

#define BUFFER_SIZE ((SCREENDIMENSIONSWIDTH / 8) * SCREENDIMENSIONSHEIGHT)	//((240 / 8) * 303) = 9090 bytes

#define MAXNAMELEN 6
#define MAXDISTANCECHARS 8 /* 'X','x','x','x',' ','f','t' */
#define MAXDISTANCEINFEET 9999 /* This can be modified */
#define MAXNAMETAGLEN (MAXNAMELEN+4) /* "H","i"," ", "X","x","x","x","x,"x" */
#define MINANGLE 0
#define MAXANGLE 360
#define MINARCLEN 0
#define MAXARCLEN 360

#define MAXTIMELENGTH 9
#define HOURSMARK 0
#define MINSMARK 3
#define SECSMARK 6
#define HOURSINTMAX 24
#define MINSINTMAX 60
#define SECONDSINTMAX 60

#define bool int /* TODO : remove this */
#define true 1 /* TODO : remove this */
#define false 0 /* TODO : remove this */

#define TABLE_PERCISION_8_BIT
// #define TABLE_PERCISION_16_BIT

#define SIN_TABLE_8_ARRAY_SIZE 91
#ifndef PI
#define PI 3.1415926535897932384626433832795
#endif
#define HALF_PI 1.5707963267948966192313216916398
#define TWO_PI 6.283185307179586476925286766559
#define DEG_TO_RAD 0.017453292519943295769236907684886
#define RAD_TO_DEG 57.295779513082320876798154814105

#define radians(deg) ((deg)*DEG_TO_RAD)
#define degrees(rad) ((rad)*RAD_TO_DEG)
//#define sq(x) ((x)*(x))

#ifdef __cplusplus
}
#endif


#endif
