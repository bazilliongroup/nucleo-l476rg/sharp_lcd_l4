/*
 * Copyright (c) 2019 Lynq Technologies
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * is NOT PERMITTED without express written consent of Lynq Technologies.
 */

/*
 * Silom12.h
 *
 *  Created on: Nov 25, 2019
 *      Author: NKP
 */
const uint8_t Silom12pt7bBitmaps[] = {
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0xCF, 0x3C, 0xF3,
  0xCF, 0x30, 0x03, 0x0C, 0x03, 0x0C, 0x06, 0x18, 0x06, 0x18, 0x3F, 0xFF,
  0x3F, 0xFF, 0x0C, 0x30, 0x0C, 0x30, 0x0C, 0x30, 0x0C, 0x30, 0xFF, 0xFC,
  0xFF, 0xFC, 0x18, 0x60, 0x18, 0x60, 0x30, 0xC0, 0x30, 0xC0, 0x0C, 0x03,
  0x03, 0xF1, 0xFE, 0xED, 0xFB, 0x3E, 0xC3, 0xF0, 0xFC, 0x1F, 0x03, 0xE0,
  0x7C, 0x0F, 0x83, 0xF0, 0xFC, 0x37, 0xCD, 0xFB, 0x77, 0xF8, 0xFC, 0x0C,
  0x03, 0x00, 0x3C, 0xFE, 0x1F, 0xBF, 0x0E, 0x70, 0xC3, 0x0C, 0x60, 0xC3,
  0x18, 0x39, 0xCC, 0x07, 0xE6, 0x00, 0xF1, 0x80, 0x00, 0xC0, 0x00, 0x30,
  0x00, 0x18, 0xF0, 0x06, 0x7E, 0x03, 0x39, 0xC0, 0xCC, 0x30, 0x63, 0x0C,
  0x30, 0xE7, 0x0C, 0x1F, 0x86, 0x03, 0xC0, 0x3F, 0xC0, 0x7F, 0xE0, 0xF9,
  0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0x78, 0x03, 0x3C, 0x03, 0x3C,
  0x03, 0x78, 0x07, 0xF0, 0xFE, 0xF0, 0xFC, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0,
  0xF0, 0xF9, 0xF0, 0x7F, 0xE0, 0x3F, 0xC0, 0xFF, 0xF0, 0x0C, 0xF7, 0x9C,
  0xF3, 0xCF, 0x3C, 0xF3, 0xCF, 0x3C, 0xF3, 0xCF, 0x3C, 0xF3, 0xC7, 0x1E,
  0x3C, 0x30, 0xC3, 0xC7, 0x8E, 0x3C, 0xF3, 0xCF, 0x3C, 0xF3, 0xCF, 0x3C,
  0xF3, 0xCF, 0x3C, 0xF3, 0x9E, 0xF3, 0x00, 0x06, 0x00, 0x60, 0x66, 0x67,
  0xFE, 0x1F, 0x81, 0xF8, 0x7F, 0xE6, 0x66, 0x06, 0x00, 0x60, 0x0C, 0x03,
  0x00, 0xC0, 0x30, 0xFF, 0xFF, 0xF0, 0xC0, 0x30, 0x0C, 0x03, 0x00, 0xFF,
  0xFF, 0x37, 0xEC, 0x00, 0xFF, 0xFF, 0xF0, 0xFF, 0xFF, 0x00, 0x60, 0x18,
  0x03, 0x00, 0xC0, 0x18, 0x03, 0x00, 0xC0, 0x18, 0x06, 0x00, 0xC0, 0x18,
  0x06, 0x00, 0xC0, 0x30, 0x06, 0x00, 0xC0, 0x30, 0x06, 0x00, 0xC0, 0x30,
  0x00, 0x3F, 0xC7, 0xFE, 0xF9, 0xFF, 0x0F, 0xF1, 0xFF, 0x1F, 0xF3, 0xFF,
  0x3F, 0xF6, 0xFF, 0x6F, 0xFC, 0xFF, 0xCF, 0xF8, 0xFF, 0x8F, 0xF0, 0xFF,
  0x9F, 0x7F, 0xE3, 0xFC, 0x1D, 0xFF, 0xFF, 0x3C, 0xF3, 0xCF, 0x3C, 0xF3,
  0xCF, 0x3C, 0xF3, 0xCF, 0x3C, 0xF0, 0x3F, 0xC7, 0xFE, 0xE1, 0xFC, 0x0F,
  0x00, 0xF0, 0x0F, 0x00, 0xF0, 0x1E, 0x03, 0xC0, 0x78, 0x0F, 0x01, 0xE0,
  0x3C, 0x07, 0x80, 0xF0, 0x0F, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
  0x01, 0xE0, 0x3C, 0x0F, 0x01, 0xE0, 0x3F, 0xC3, 0xFE, 0x01, 0xF0, 0x0F,
  0x00, 0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0xC0, 0xFE, 0x1F, 0x7F, 0xE3, 0xFC,
  0x00, 0xF0, 0x07, 0xC0, 0x3F, 0x01, 0xBC, 0x0C, 0xF0, 0x33, 0xC1, 0x8F,
  0x0C, 0x3C, 0x60, 0xF3, 0x03, 0xCF, 0xFF, 0xFF, 0xFF, 0x00, 0xF0, 0x03,
  0xC0, 0x0F, 0x00, 0x3C, 0x00, 0xF0, 0x03, 0xC0, 0xFF, 0xFF, 0xFF, 0xF0,
  0x0F, 0x00, 0xF0, 0x0F, 0x00, 0xFF, 0xCF, 0xFE, 0x01, 0xF0, 0x0F, 0x00,
  0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0xC0, 0xFE, 0x1F, 0x7F, 0xE3, 0xFC, 0x0F,
  0xC1, 0xFC, 0x3C, 0x07, 0x80, 0xF0, 0x0F, 0x00, 0xF7, 0xCF, 0xFE, 0xF9,
  0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x9F, 0x7F,
  0xE3, 0xFC, 0xFF, 0xFF, 0xFF, 0x00, 0xF0, 0x0F, 0x00, 0xF0, 0x1E, 0x01,
  0xE0, 0x3C, 0x03, 0xC0, 0x78, 0x07, 0x80, 0xF8, 0x0F, 0x00, 0xF0, 0x0F,
  0x00, 0xF0, 0x0F, 0x00, 0xF0, 0x3F, 0xC7, 0xFE, 0xF9, 0xFF, 0x0F, 0xF0,
  0xFF, 0x0F, 0xF0, 0xF7, 0x9E, 0x3F, 0xC3, 0xFC, 0x71, 0xEF, 0x0F, 0xF0,
  0xFF, 0x0F, 0xF0, 0xFF, 0x9F, 0x7F, 0xE3, 0xFC, 0x3F, 0xC7, 0xFE, 0xF9,
  0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x9F, 0x7F,
  0xF3, 0xEF, 0x00, 0xF0, 0x0F, 0x01, 0xE0, 0x3C, 0x3F, 0x83, 0xF0, 0xFF,
  0xFF, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0xFF,
  0xFF, 0x37, 0xEC, 0x00, 0x00, 0xE0, 0x18, 0x07, 0x00, 0xE0, 0x1C, 0x03,
  0x80, 0x70, 0x07, 0x00, 0x38, 0x01, 0xC0, 0x0E, 0x00, 0x70, 0x01, 0x80,
  0x0E, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x70, 0x03,
  0x80, 0x1C, 0x00, 0x60, 0x03, 0x80, 0x1C, 0x00, 0xE0, 0x0E, 0x01, 0xC0,
  0x38, 0x0E, 0x01, 0xC0, 0x38, 0x07, 0x00, 0x3F, 0xC7, 0xFE, 0xE1, 0xFC,
  0x0F, 0x00, 0xF0, 0x1F, 0x03, 0xE0, 0x7C, 0x0F, 0x00, 0xF0, 0x0F, 0x00,
  0xF0, 0x00, 0x00, 0x00, 0x0F, 0x00, 0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0xFC,
  0x07, 0xFF, 0x83, 0x80, 0x71, 0x80, 0x06, 0xE3, 0xF1, 0xF1, 0xFC, 0x3C,
  0xE3, 0x0F, 0x30, 0xC3, 0xCC, 0x30, 0xF3, 0x0C, 0x3C, 0xC3, 0x0F, 0x39,
  0xC3, 0xC7, 0xFF, 0xF8, 0xE7, 0xE6, 0x00, 0x00, 0xE0, 0x00, 0x1F, 0xFC,
  0x03, 0xFF, 0x00, 0x3F, 0xC7, 0xFE, 0xF9, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F,
  0xF0, 0xFF, 0x0F, 0xFF, 0xFF, 0xFF, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F,
  0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xFF, 0xCF, 0xFE, 0xF1, 0xFF, 0x0F,
  0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x1E, 0xFF, 0xCF, 0xFC, 0xF1, 0xEF, 0x0F,
  0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x1F, 0xFF, 0xEF, 0xFC, 0x3F, 0xC7, 0xFE,
  0xF8, 0x6F, 0x02, 0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0x00,
  0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0x00, 0xF0, 0x2F, 0x86, 0x7F, 0xE3, 0xFC,
  0xFF, 0xCF, 0xFE, 0xF1, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F,
  0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x1F,
  0xFF, 0xEF, 0xFC, 0xFF, 0xFF, 0xFF, 0x03, 0xC0, 0xF0, 0x3C, 0x0F, 0x03,
  0xC0, 0xFF, 0x3F, 0xCF, 0x03, 0xC0, 0xF0, 0x3C, 0x0F, 0x03, 0xC0, 0xFF,
  0xFF, 0xF0, 0xFF, 0xFF, 0xFF, 0x03, 0xC0, 0xF0, 0x3C, 0x0F, 0x03, 0xC0,
  0xFF, 0x3F, 0xCF, 0x03, 0xC0, 0xF0, 0x3C, 0x0F, 0x03, 0xC0, 0xF0, 0x3C,
  0x00, 0x3F, 0xC7, 0xFE, 0xF8, 0x7F, 0x03, 0xF0, 0x0F, 0x00, 0xF0, 0x0F,
  0x00, 0xF3, 0xFF, 0x3F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF,
  0x9F, 0x7F, 0xE3, 0xFC, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF,
  0x0F, 0xF0, 0xFF, 0x0F, 0xFF, 0xFF, 0xFF, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF,
  0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0x00, 0xF0,
  0x0F, 0x00, 0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF,
  0x0F, 0xF0, 0xFF, 0x9F, 0x7F, 0xE3, 0xFC, 0xF0, 0x3B, 0xC1, 0xEF, 0x0F,
  0x3C, 0x78, 0xF3, 0xC3, 0xCE, 0x0F, 0x70, 0x3F, 0x80, 0xFC, 0x03, 0xF0,
  0x0F, 0xE0, 0x3F, 0xC0, 0xF7, 0x83, 0xCF, 0x0F, 0x1E, 0x3C, 0x3C, 0xF0,
  0x73, 0xC1, 0xE0, 0xF0, 0x3C, 0x0F, 0x03, 0xC0, 0xF0, 0x3C, 0x0F, 0x03,
  0xC0, 0xF0, 0x3C, 0x0F, 0x03, 0xC0, 0xF0, 0x3C, 0x0F, 0x03, 0xC0, 0xFF,
  0xFF, 0xF0, 0x80, 0x00, 0x7C, 0x00, 0x0F, 0xE0, 0x01, 0xFF, 0x00, 0x3F,
  0xF8, 0x07, 0xFF, 0xC0, 0xFF, 0xFE, 0x1F, 0xFF, 0xF3, 0xFF, 0xDF, 0xFE,
  0xFC, 0xFF, 0xCF, 0xC7, 0xF8, 0xFC, 0x3F, 0x0F, 0xC1, 0xE0, 0xFC, 0x08,
  0x0F, 0xC0, 0x00, 0xFC, 0x00, 0x0F, 0xC0, 0x00, 0xFC, 0x00, 0x0F, 0x80,
  0x0F, 0x80, 0x3F, 0x00, 0xFE, 0x03, 0xFC, 0x0F, 0xF8, 0x3F, 0xF0, 0xFF,
  0xE3, 0xDF, 0xCF, 0x3F, 0xBC, 0x7F, 0xF0, 0xFF, 0xC1, 0xFF, 0x03, 0xFC,
  0x07, 0xF0, 0x0F, 0xC0, 0x1F, 0x00, 0x10, 0x3F, 0xC7, 0xFE, 0xF9, 0xFF,
  0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF,
  0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x9F, 0x7F, 0xE3, 0xFC, 0xFF, 0xCF,
  0xFE, 0xF1, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x1F, 0xFF, 0xEF,
  0xFC, 0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0x00, 0xF0, 0x0F,
  0x00, 0x3F, 0xC7, 0xFE, 0xF9, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF,
  0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF,
  0x9F, 0x7F, 0xE3, 0xFC, 0x01, 0xF0, 0x0F, 0xFF, 0xCF, 0xFE, 0xF1, 0xFF,
  0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x1E, 0xFF, 0xCF, 0xFE, 0xF1, 0xFF,
  0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0x3F, 0x1F,
  0xEF, 0x9F, 0xC3, 0xF0, 0x3E, 0x0F, 0xC1, 0xF0, 0x3E, 0x07, 0xC0, 0xF8,
  0x3F, 0x07, 0xC0, 0xFC, 0x3F, 0x9F, 0x7F, 0x8F, 0xC0, 0xFF, 0xFF, 0xFF,
  0x0F, 0x00, 0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0x00, 0xF0,
  0x0F, 0x00, 0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0x00, 0xF0,
  0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F,
  0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x9F,
  0x7F, 0xE3, 0xFC, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F,
  0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F,
  0xF0, 0xEF, 0x1C, 0xFF, 0x8F, 0xF0, 0xF0, 0xF0, 0xFF, 0x0F, 0x0F, 0xF0,
  0xF0, 0xFF, 0x0F, 0x0F, 0xF0, 0xF0, 0xFF, 0x0F, 0x0F, 0xF0, 0xF0, 0xFF,
  0x0F, 0x0F, 0xF0, 0xF0, 0xFF, 0x0F, 0x0F, 0xF0, 0xF0, 0xFF, 0x0F, 0x0F,
  0xF0, 0xF0, 0xFF, 0x0F, 0x0F, 0xF0, 0xF0, 0xEF, 0x1F, 0x1C, 0xFF, 0xFF,
  0x8F, 0xFF, 0xF0, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F,
  0xF0, 0xF7, 0x9E, 0x3F, 0xC3, 0xFC, 0x79, 0xEF, 0x0F, 0xF0, 0xFF, 0x0F,
  0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F,
  0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x9F, 0x7F, 0xE3, 0xFC, 0x0F, 0x00, 0xF0,
  0x0F, 0x00, 0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0x00, 0xF0, 0xFF, 0xFF, 0xFF,
  0x00, 0xF0, 0x1E, 0x01, 0xE0, 0x3C, 0x07, 0x80, 0x78, 0x0F, 0x00, 0xF0,
  0x1E, 0x01, 0xE0, 0x3C, 0x07, 0x80, 0x78, 0x0F, 0x00, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0x3C, 0xF3, 0xCF, 0x3C, 0xF3, 0xCF, 0x3C, 0xF3, 0xCF, 0x3C,
  0xF3, 0xCF, 0x3C, 0xFF, 0xF0, 0x60, 0x06, 0x00, 0xC0, 0x18, 0x01, 0x80,
  0x30, 0x06, 0x00, 0x60, 0x0C, 0x00, 0xC0, 0x18, 0x03, 0x00, 0x30, 0x06,
  0x00, 0x60, 0x0C, 0x01, 0x80, 0x18, 0x03, 0x00, 0x30, 0xFF, 0xF3, 0xCF,
  0x3C, 0xF3, 0xCF, 0x3C, 0xF3, 0xCF, 0x3C, 0xF3, 0xCF, 0x3C, 0xF3, 0xCF,
  0xFF, 0xF0, 0x02, 0x00, 0x38, 0x03, 0x60, 0x31, 0x83, 0x06, 0x30, 0x18,
  0xFF, 0xFF, 0xFF, 0xFF, 0x61, 0x8C, 0x30, 0x3F, 0xC7, 0xFE, 0xE1, 0xFC,
  0x0F, 0x00, 0xF0, 0x0F, 0x3F, 0xF7, 0xFF, 0xF8, 0xFF, 0x0F, 0xF0, 0xFF,
  0x9F, 0x7F, 0xF3, 0xEF, 0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0x00, 0xF7, 0xCF,
  0xFE, 0xF9, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF,
  0x0F, 0xF0, 0xFF, 0x1F, 0xFF, 0xEF, 0xFC, 0x3F, 0x1F, 0xEF, 0x9B, 0xC2,
  0xF0, 0x3C, 0x0F, 0x03, 0xC0, 0xF0, 0x3C, 0x0F, 0x0B, 0xE6, 0x7F, 0x8F,
  0xC0, 0x00, 0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0x3F, 0xF7, 0xFF, 0xF8, 0xFF,
  0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF,
  0x9F, 0x7F, 0xF3, 0xEF, 0x3F, 0xC7, 0xFE, 0xF9, 0xFF, 0x0F, 0xF0, 0xFF,
  0x0F, 0xFF, 0xFF, 0xFF, 0xF0, 0x0F, 0x00, 0xF0, 0x3F, 0x87, 0x7F, 0xE3,
  0xFC, 0x0F, 0xC7, 0xF3, 0xE0, 0xF0, 0xFF, 0x3F, 0xC3, 0xC0, 0xF0, 0x3C,
  0x0F, 0x03, 0xC0, 0xF0, 0x3C, 0x0F, 0x03, 0xC0, 0xF0, 0x3C, 0x0F, 0x00,
  0x3F, 0xF7, 0xFF, 0xF8, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F,
  0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x9F, 0x7F, 0xF3, 0xEF, 0x00, 0xF0, 0x0F,
  0xC0, 0xFE, 0x1F, 0x7F, 0xE3, 0xFC, 0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0x00,
  0xF7, 0xCF, 0xFE, 0xF9, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F,
  0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xFF, 0x00, 0xFF,
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x03, 0xC0, 0xF0, 0x00, 0x00, 0x03,
  0xC0, 0xF0, 0x3C, 0x0F, 0x03, 0xC0, 0xF0, 0x3C, 0x0F, 0x03, 0xC0, 0xF0,
  0x3C, 0x0F, 0x03, 0xC0, 0xFC, 0x3F, 0x9F, 0x7F, 0x8F, 0xC0, 0xF0, 0x03,
  0xC0, 0x0F, 0x00, 0x3C, 0x00, 0xF0, 0x7B, 0xC3, 0xCF, 0x1E, 0x3C, 0xF0,
  0xF7, 0x83, 0xF8, 0x0F, 0xC0, 0x3F, 0x00, 0xFE, 0x03, 0xDE, 0x0F, 0x3C,
  0x3C, 0x78, 0xF0, 0xF3, 0xC1, 0xE0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0xFF, 0xF7, 0xCF, 0xCF, 0xFF, 0xFE, 0xF9, 0xF9, 0xFF, 0x0F,
  0x0F, 0xF0, 0xF0, 0xFF, 0x0F, 0x0F, 0xF0, 0xF0, 0xFF, 0x0F, 0x0F, 0xF0,
  0xF0, 0xFF, 0x0F, 0x0F, 0xF0, 0xF0, 0xFF, 0x0F, 0x0F, 0xF0, 0xF0, 0xFF,
  0x0F, 0x0F, 0xF7, 0xCF, 0xFE, 0xF9, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0,
  0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0x3F,
  0xC7, 0xFE, 0xF9, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0,
  0xFF, 0x0F, 0xF0, 0xFF, 0x9F, 0x7F, 0xE3, 0xFC, 0xFF, 0xCF, 0xFE, 0xF9,
  0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0,
  0xFF, 0x1F, 0xFF, 0xEF, 0xFC, 0xF0, 0x0F, 0x00, 0xF0, 0x0F, 0x00, 0x3F,
  0xF7, 0xFF, 0xF8, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0,
  0xFF, 0x0F, 0xF0, 0xFF, 0x9F, 0x7F, 0xF3, 0xEF, 0x00, 0xF0, 0x0F, 0x00,
  0xF0, 0x0F, 0xF3, 0xFD, 0xFF, 0xFF, 0xFF, 0xF8, 0x3C, 0x0F, 0x03, 0xC0,
  0xF0, 0x3C, 0x0F, 0x03, 0xC0, 0xF0, 0x3C, 0x00, 0x3F, 0x1F, 0xEF, 0x1F,
  0xC3, 0xF0, 0x3F, 0x07, 0xF0, 0xFE, 0x0F, 0xC1, 0xFC, 0x3F, 0x8F, 0x7F,
  0x8F, 0xC0, 0x3C, 0x3C, 0x3C, 0x3C, 0xFF, 0xFF, 0x3C, 0x3C, 0x3C, 0x3C,
  0x3C, 0x3C, 0x3C, 0x3C, 0x3C, 0x3E, 0x1F, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0,
  0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0,
  0xFF, 0x9F, 0x7F, 0xF3, 0xEF, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0,
  0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xEF, 0x1C, 0xFF,
  0x8F, 0xF0, 0xF0, 0xF0, 0xFF, 0x0F, 0x0F, 0xF0, 0xF0, 0xFF, 0x0F, 0x0F,
  0xF0, 0xF0, 0xFF, 0x0F, 0x0F, 0xF0, 0xF0, 0xFF, 0x0F, 0x0F, 0xF0, 0xF0,
  0xFF, 0x0F, 0x0F, 0xF0, 0xF0, 0xEF, 0x1F, 0x1C, 0xFF, 0xFF, 0x8F, 0xFF,
  0xF0, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xF7, 0x9E, 0x3F, 0xC3,
  0xFC, 0x79, 0xEF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF,
  0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF, 0x0F, 0xF0, 0xFF,
  0x0F, 0xF0, 0xFF, 0x9F, 0x7F, 0xF3, 0xEF, 0x00, 0xF0, 0x0F, 0xC0, 0xFE,
  0x1F, 0x7F, 0xE3, 0xFC, 0xFF, 0xFF, 0xFF, 0x00, 0xF0, 0x1E, 0x03, 0xC0,
  0x78, 0x0F, 0x00, 0xF0, 0x1E, 0x03, 0xC0, 0x78, 0x0F, 0x00, 0xFF, 0xFF,
  0xFF, 0x0C, 0x73, 0x8C, 0x30, 0xC3, 0x0C, 0x30, 0xCE, 0x38, 0x30, 0xC3,
  0x0C, 0x30, 0xC3, 0x0E, 0x1C, 0x30, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xF0,
  0xC3, 0x87, 0x0C, 0x30, 0xC3, 0x0C, 0x30, 0xC1, 0xC7, 0x30, 0xC3, 0x0C,
  0x30, 0xC3, 0x1C, 0xE3, 0x00, 0x1C, 0x19, 0xF9, 0x9C, 0xFC, 0xC3, 0xC0
};

const Graphics::GFX_GLYPH Silom12pt7bGlyphs[] = {
  {     0,   0,   0,   6,    0,    1 },   // 0x20 ' '
  {     0,   4,  18,  12,    4,  -17 },   // 0x21 '!'
  {     9,   6,   6,  14,    4,  -17 },   // 0x22 '"'
  {    14,  16,  16,  20,    2,  -17 },   // 0x23 '#'
  {    46,  10,  22,  14,    2,  -19 },   // 0x24 '$'
  {    74,  18,  18,  22,    2,  -17 },   // 0x25 '%'
  {   115,  16,  18,  20,    2,  -17 },   // 0x26 '&'
  {   151,   2,   6,   6,    2,  -17 },   // 0x27 '''
  {   153,   6,  22,  10,    2,  -19 },   // 0x28 '('
  {   170,   6,  22,  10,    2,  -19 },   // 0x29 ')'
  {   187,  12,  10,  14,    1,  -16 },   // 0x2A '*'
  {   202,  10,  10,  14,    2,  -11 },   // 0x2B '+'
  {   215,   4,   9,   8,    2,   -3 },   // 0x2C ','
  {   220,  10,   2,  14,    2,   -7 },   // 0x2D '-'
  {   223,   4,   4,   8,    2,   -3 },   // 0x2E '.'
  {   225,  11,  20,  14,    1,  -19 },   // 0x2F '/'
  {   253,  12,  18,  16,    2,  -17 },   // 0x30 '0'
  {   280,   6,  18,  16,    4,  -17 },   // 0x31 '1'
  {   294,  12,  18,  16,    2,  -17 },   // 0x32 '2'
  {   321,  12,  18,  16,    2,  -17 },   // 0x33 '3'
  {   348,  14,  18,  16,    0,  -17 },   // 0x34 '4'
  {   380,  12,  18,  16,    2,  -17 },   // 0x35 '5'
  {   407,  12,  18,  16,    2,  -17 },   // 0x36 '6'
  {   434,  12,  18,  16,    2,  -17 },   // 0x37 '7'
  {   461,  12,  18,  16,    2,  -17 },   // 0x38 '8'
  {   488,  12,  18,  16,    2,  -17 },   // 0x39 '9'
  {   515,   4,  14,   8,    2,  -13 },   // 0x3A ':'
  {   522,   4,  19,   8,    2,  -13 },   // 0x3B ';'
  {   532,  12,  14,  12,    0,  -15 },   // 0x3C '<'
  {   553,  12,   6,  16,    2,   -9 },   // 0x3D '='
  {   562,  12,  14,  12,    0,  -15 },   // 0x3E '>'
  {   583,  12,  18,  16,    2,  -17 },   // 0x3F '?'
  {   610,  18,  18,  22,    2,  -17 },   // 0x40 '@'
  {   651,  12,  18,  16,    2,  -17 },   // 0x41 'A'
  {   678,  12,  18,  16,    2,  -17 },   // 0x42 'B'
  {   705,  12,  18,  16,    2,  -17 },   // 0x43 'C'
  {   732,  12,  18,  16,    2,  -17 },   // 0x44 'D'
  {   759,  10,  18,  14,    2,  -17 },   // 0x45 'E'
  {   782,  10,  18,  14,    2,  -17 },   // 0x46 'F'
  {   805,  12,  18,  16,    2,  -17 },   // 0x47 'G'
  {   832,  12,  18,  16,    2,  -17 },   // 0x48 'H'
  {   859,   4,  18,  12,    4,  -17 },   // 0x49 'I'
  {   868,  12,  18,  14,    0,  -17 },   // 0x4A 'J'
  {   895,  14,  18,  18,    2,  -17 },   // 0x4B 'K'
  {   927,  10,  18,  14,    2,  -17 },   // 0x4C 'L'
  {   950,  20,  18,  24,    2,  -17 },   // 0x4D 'M'
  {   995,  14,  18,  18,    2,  -17 },   // 0x4E 'N'
  {  1027,  12,  18,  16,    2,  -17 },   // 0x4F 'O'
  {  1054,  12,  18,  16,    2,  -17 },   // 0x50 'P'
  {  1081,  12,  20,  16,    2,  -17 },   // 0x51 'Q'
  {  1111,  12,  18,  16,    2,  -17 },   // 0x52 'R'
  {  1138,  10,  18,  14,    2,  -17 },   // 0x53 'S'
  {  1161,  12,  18,  12,    0,  -17 },   // 0x54 'T'
  {  1188,  12,  18,  16,    2,  -17 },   // 0x55 'U'
  {  1215,  12,  18,  16,    2,  -17 },   // 0x56 'V'
  {  1242,  20,  18,  24,    2,  -17 },   // 0x57 'W'
  {  1287,  12,  18,  16,    2,  -17 },   // 0x58 'X'
  {  1314,  12,  18,  16,    2,  -17 },   // 0x59 'Y'
  {  1341,  12,  18,  16,    2,  -17 },   // 0x5A 'Z'
  {  1368,   6,  22,  10,    2,  -19 },   // 0x5B '['
  {  1385,  11,  20,  14,    1,  -19 },   // 0x5C '\'
  {  1413,   6,  22,  10,    2,  -19 },   // 0x5D ']'
  {  1430,  13,   6,  16,    1,  -17 },   // 0x5E '^'
  {  1440,  16,   2,  16,    0,    1 },   // 0x5F '_'
  {  1444,   5,   4,  12,    3,  -19 },   // 0x60 '`'
  {  1447,  12,  14,  16,    2,  -13 },   // 0x61 'a'
  {  1468,  12,  18,  16,    2,  -17 },   // 0x62 'b'
  {  1495,  10,  14,  14,    2,  -13 },   // 0x63 'c'
  {  1513,  12,  18,  16,    2,  -17 },   // 0x64 'd'
  {  1540,  12,  14,  16,    2,  -13 },   // 0x65 'e'
  {  1561,  10,  18,  12,    2,  -17 },   // 0x66 'f'
  {  1584,  12,  20,  16,    2,  -13 },   // 0x67 'g'
  {  1614,  12,  18,  16,    2,  -17 },   // 0x68 'h'
  {  1641,   4,  18,   8,    2,  -17 },   // 0x69 'i'
  {  1650,  10,  22,  12,    0,  -17 },   // 0x6A 'j'
  {  1678,  14,  18,  16,    2,  -17 },   // 0x6B 'k'
  {  1710,   4,  18,   8,    2,  -17 },   // 0x6C 'l'
  {  1719,  20,  14,  24,    2,  -13 },   // 0x6D 'm'
  {  1754,  12,  14,  16,    2,  -13 },   // 0x6E 'n'
  {  1775,  12,  14,  16,    2,  -13 },   // 0x6F 'o'
  {  1796,  12,  18,  16,    2,  -13 },   // 0x70 'p'
  {  1823,  12,  18,  16,    2,  -13 },   // 0x71 'q'
  {  1850,  10,  14,  12,    2,  -13 },   // 0x72 'r'
  {  1868,  10,  14,  14,    2,  -13 },   // 0x73 's'
  {  1886,   8,  18,  12,    2,  -17 },   // 0x74 't'
  {  1904,  12,  14,  16,    2,  -13 },   // 0x75 'u'
  {  1925,  12,  14,  16,    2,  -13 },   // 0x76 'v'
  {  1946,  20,  14,  24,    2,  -13 },   // 0x77 'w'
  {  1981,  12,  14,  16,    2,  -13 },   // 0x78 'x'
  {  2002,  12,  20,  16,    2,  -13 },   // 0x79 'y'
  {  2032,  12,  14,  16,    2,  -13 },   // 0x7A 'z'
  {  2053,   6,  22,  10,    2,  -19 },   // 0x7B '{'
  {  2070,   2,  22,  10,    4,  -19 },   // 0x7C '|'
  {  2076,   6,  22,  10,    2,  -19 },   // 0x7D '}'
  {  2093,  13,   4,  16,    1,  -13 }
}; // 0x7E '~'

const Graphics::GFX_FONT Silom12pt7b = {
  (uint8_t  *)Silom12pt7bBitmaps,
  (Graphics::GFX_GLYPH *)Silom12pt7bGlyphs,
  0x20, 0x7E, 30
};
