/*
Name:				app_gui_gfx.c
Library:			LCD Driver for STM32 MCUs
Written by:			Ronald Bazillion
Date written:		02/13/2020
Description:		This is the lcd implementation file for the sharp LS018B7DH02

                    The Sharp display sends the data in LSB Least Significant Bit format.

Resources:

Modifications:
    02/13/2020      Ronald Bazillion        First Draft

-----------------
*/

//***** Header files *****//
#include <string.h>
#include "stdbool.h"
#include "app_gui_gfx.h"
#include "lcd_sharp.h"
#include "lcd_graphics_prototypes.h"

#include "images.inc"

//***** typedefs defines *****//
#define CIRCLE_X_LOCATION 115
#define CIRCLE_Y_LOCATION 120
#define CIRCLE_RADIUS 110
#define BOLDARC_RADIUS 110

//***** typedefs enums *****//

//***** typedefs structs *****//


//***** static Library variables *****//
//****Remember "static" is used like "private" in C++*******//

//*********static function prototypes***********************//
//****Remember "static" is used like "private" in C++*******//

//***** API Functions definition *****//
//****Remember "API" is used like "public" in C++*******//

void APP_GUI_Location(void)
{
	Graphics_clearRect();				//clear rectangle based upon graphics invert Display Flag

	Graphics_setCursor(16, 50);
	uint8_t myBuffer1[] = "OTTO Location";
	LynqGraphics_writeStream(&myBuffer1[0], 13);

	Graphics_setTextSize(2);						//set to bigger text size
	Graphics_setCursor(8, 100);
	uint8_t myBuffer2[] = "graphics";
	LynqGraphics_writeStream(&myBuffer2[0], 8);

	Graphics_setTextSize(1);						//reset to default text size
	Graphics_setCursor(20, 200);
	uint8_t myBuffer3[] = "SEL to ent/ex";
	LynqGraphics_writeStream(&myBuffer3[0], 13);

	Graphics_setCursor(20, 225);
	uint8_t myBuffer4[] = "UP to next";
	LynqGraphics_writeStream(&myBuffer4[0], 10);

	Graphics_setCursor(20, 250);
	uint8_t myBuffer5[] = "DWN to prev";
	LynqGraphics_writeStream(&myBuffer5[0], 11);

	//reset cursor values
	Graphics_setCursor(0, 0);

	LCD_Sharp_MvGraphicsDisplayToBuffer();

	LCD_Sharp_SendData();
}

void APP_GUI_AlanGraphic(void)
{
	// Draws Device Team Member Screen displayBuffer
	//
	Graphics_clearRect();				//clear rectangle based upon graphics invert Display Flag

	uint16_t angle = 137 % 360;			//In degrees
	uint16_t arcLength = 65 % 360;		//In degrees
	LynqGraphics_drawShapeBoldArc(0, CIRCLE_X_LOCATION, CIRCLE_Y_LOCATION, BOLDARC_RADIUS, angle, arcLength);
	LynqGraphics_drawShapeCircle(0, CIRCLE_X_LOCATION, CIRCLE_Y_LOCATION, CIRCLE_RADIUS);

    Graphics_setTextSize(2);
    LynqGraphics_setCursorOffset(-15, 90);					//Custom to put in center of circle

    LynqGraphics_setLineNum(1);								//set offset for distance location
    uint16_t distance = 137; /* Max length 4, Refer MAXDISTANCECHARS */
    LynqGraphics_setDistance(distance);/* Set distance in Feet */

    LynqGraphics_drawShapeFillRect(0, 0, 250, 115, 52);
    LynqGraphics_drawShapeFillRect(0, 0, 248, 230, 2);

    char operator1[] = "Alan";
	uint16_t operator1Len = 4 % 6;				//MAX 5 CHARACTERS
	LynqGraphics_setLineNum(5);								//set offset for name location
    LynqGraphics_setName(&operator1[0], operator1Len);

    Graphics_setTextSize(1);								//resetting text size and offset
    LynqGraphics_setCursorOffset(60, 200);

    //sending Alan again
    LynqGraphics_setName(&operator1[0], operator1Len);

    if (Graphics_getInvertDisplay()) { Graphics_setInvertDisplay(false); }	//invert the FG & BG settings
    else { Graphics_setInvertDisplay(true); }

    char operator2[] = "Shaun";
	uint16_t operator2Len = 5 % 6;
    LynqGraphics_setCursorOffset(-70, 200);
    LynqGraphics_setName(&operator2[0], operator2Len);

    if (Graphics_getInvertDisplay()) { Graphics_setInvertDisplay(false); }	//reset FG & BG settings
    else { Graphics_setInvertDisplay(true); }

    //Reset offsets
    LynqGraphics_setCursorOffset(0, 0);
    Graphics_setTextSize(1);
    LynqGraphics_setLineNum(0);

	LCD_Sharp_MvGraphicsDisplayToBuffer();
	LCD_Sharp_SendData();
}



void APP_GUI_ShaunGraphic(void)
{
	// Draws Device Team Member Screen displayBuffer
	//
	Graphics_clearRect();				//clear rectangle based upon graphics invert Display Flag

	uint16_t angle = 45 % 360;			//In degrees
	uint16_t arcLength = 13 % 360;		//In degrees
	LynqGraphics_drawShapeBoldArc(0, CIRCLE_X_LOCATION, CIRCLE_Y_LOCATION, BOLDARC_RADIUS, angle, arcLength);
    LynqGraphics_drawShapeCircle(0, CIRCLE_X_LOCATION, CIRCLE_Y_LOCATION, CIRCLE_RADIUS);

    Graphics_setTextSize(2);
    LynqGraphics_setCursorOffset(-15, 90);					//Custom to put in center of circle

    LynqGraphics_setLineNum(1);								//set offset for distance location
    uint16_t distance = 514; /* Max length 4, Refer MAXDISTANCECHARS */
    LynqGraphics_setDistance(distance);/* Set distance in Feet */

    LynqGraphics_drawShapeFillRect(0, 115, 250, 115, 52);
    LynqGraphics_drawShapeFillRect(0, 0, 248, 230, 2);

    char operator1[] = "Shaun";
	uint16_t operator1Len = 5 % 6;				//MAX 5 CHARACTERS
	LynqGraphics_setLineNum(5);								//set offset for name location
    LynqGraphics_setName(&operator1[0], operator1Len);

    Graphics_setTextSize(1);								//resetting text size and offset
    LynqGraphics_setCursorOffset(60, 200);

    if (Graphics_getInvertDisplay()) { Graphics_setInvertDisplay(false); }	//invert the FG & BG settings
     else { Graphics_setInvertDisplay(true); }

    char operator2[] = "Alan";
    uint16_t operator2Len = 4 % 6;
    LynqGraphics_setName(&operator2[0], operator2Len);

    if (Graphics_getInvertDisplay()) { Graphics_setInvertDisplay(false); }		//reset FG & BG settings
    else { Graphics_setInvertDisplay(true); }

    char operator3[] = "Shaun";
	uint16_t operator3Len = 5 % 6;
    LynqGraphics_setCursorOffset(-70, 200);
    LynqGraphics_setName(&operator3[0], operator3Len);

    //Reset offsets
    LynqGraphics_setCursorOffset(0, 0);
    Graphics_setTextSize(1);
    LynqGraphics_setLineNum(0);

	LCD_Sharp_MvGraphicsDisplayToBuffer();
	LCD_Sharp_SendData();
}

//************************************************** STATIC FUNCTIONS ***********************************************
//*******************************************************************************************************************
//*******************************************************************************************************************
