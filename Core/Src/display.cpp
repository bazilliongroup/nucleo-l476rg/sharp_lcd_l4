/*
 * Copyright (c) 2019 Lynq TechnologiesinitGraphics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * is NOT PERMITTED without express written consent of Lynq Technologies.
 */

/*
 * Display.cpp
 *
 *  Created on: Nov 25, 2019
 *      Author: NKP
 */

#include "display.h"
#include "fast_math.h"
#include "lynq_label.h"
#include "graphics.h"

#define X_CENTER 115
#define Y_CENTER 111
#define CIRCLE_RADIUS 5
#define CENTER_RADIUS 56

/**************************************************************************/
/*!
   @brief    Constructor for Display Object
   @param    None
 */
/**************************************************************************/
Display::Display(void) { }

/**************************************************************************/
/*!
   @brief    Initialize the base parameters of the display variables here
   @param    None
 */
/**************************************************************************/
void Display::initDisplay(void)
{
    g_Graphics.initGraphics();
    resolution.width = SCREENDIMENSIONSWIDTH;/* number of width bits */
    resolution.height = SCREENDIMENSIONSHEIGHT;/* number of height bits */
    fontWidth = g_Graphics.getFontYAdvance();/* Widest character spacing */
    fontHeight = g_Graphics.getFontXAdvance();/* Tallest character spacing */
    setCursorOffset(0,0);
    setLineNum(1);
}

/**************************************************************************/
/*!
   @brief    Draws an arc with a set specific radius
   @param    None
 */
/**************************************************************************/
void Display::drawArc(uint16_t angle, uint16_t arcLength)
{
    ShapeArc_t tmpArc = {CENTER_RADIUS,angle,arcLength};
    drawShape(SHAPE_BoldArc,0,&tmpArc);
}

/**************************************************************************/
/*!
   @brief    Prints the time along with the user's name on the screen
   @param    namePtr - pointer to the name buffer
   @param    nameLen - size of array
   @param    time - time information whcih is based upon hours, minutes, seconds
 */
/**************************************************************************/
void Display::drawMainScreen(char *namePtr, uint16_t nameLen, Time_t* time)
{
    drawTime(time);
    drawNameWithTag(namePtr, nameLen);
}

/**************************************************************************/
/*!
   @brief    Prints the arrow-graphic with group-member's name and distance on the screen
   @param    namePtr - pointer to the name buffer
   @param    nameLenIn - size of array
   @param    angleIn - angle of the arc (0 - 360 degrees)
   @param    arcLengthIn - arclength (0 - 360 degrees)
   @param    distance - distance measurd in feet

   NOTE: 1) Maximum length of nameLenIn should be 6 characters
         2) You need to configure the line Number using setLineNum(uint8_t lineNum)
 */
/**************************************************************************/
void Display::drawMemberScreen(char *namePtr, uint16_t nameLenIn, uint16_t angleIn, uint16_t arcLengthIn, uint16_t distance)
{
    //  uint16_t nameLen = nameLenIn % MAXNAMELEN;
    uint16_t nameLen = nameLenIn;
    uint16_t angle = angleIn % MAXANGLE;
    uint16_t arcLength = arcLengthIn % MAXARCLEN;

    drawArc(angle,arcLength);
    setDistance(distance, m_cursorOffsetX, m_cursorOffsetY);/* Set distance in Feet */
    setName(namePtr, nameLen);
}

/**************************************************************************/
/*!
   @brief    Draws the time
   @param    timeIn - Time structure (hours, minutes, seconds)
 */
/**************************************************************************/
void Display::drawTime(Time_t* timeIn)
{
    Time_t time;
    time.hours = timeIn->hours % HOURSINTMAX;
    time.mins = timeIn->mins % MINSINTMAX;
    time.seconds = timeIn->mins % SECONDSINTMAX;

    char temp[MAXTIMELENGTH];
    memset(temp,'\0',MAXTIMELENGTH);

    if(time.hours < 10)
        sprintf(temp+HOURSMARK,"0%d:",time.hours);
    else
        sprintf(temp+HOURSMARK,"%d:",time.hours);

#ifdef DEBUGLOW
    printf("\nTime = %s\n",temp);
#endif
    if(time.mins < 10)
        sprintf(temp+MINSMARK,"0%d:",time.mins);
    else
        sprintf(temp+MINSMARK,"%d:",time.mins);

#ifdef DEBUGLOW
    printf("\nTime = %s\n",temp);
#endif
    if(time.seconds < 10)
        sprintf(temp+SECSMARK,"0%d",time.seconds);
    else
        sprintf(temp+SECSMARK,"%d",time.seconds);

#ifdef DEBUG
    printf("\nTime = %s\n",temp);
#endif
    centerTextCust(temp,MAXTIMELENGTH,3);
}

/**************************************************************************/
/*!
   @brief    Draws the name with the tag
   @param    namePtr - Name pointer to array
   @param    nameLen - Name Length

   NOTE: 1)  nameLen must be <= 6 bytes
 */
/**************************************************************************/
void Display::drawNameWithTag(char *namePtr, uint16_t nameLen)
{
    if ((nameLen < 1) || (nameLen > 6))
        return;

    char temp[MAXNAMETAGLEN];
    memset(temp,'\0',MAXTIMELENGTH);
    sprintf(temp,"Hi %s",namePtr);

    printf("\nName Tag = %s\n",temp);
    centerTextCust(temp,MAXTIMELENGTH,5);
}


/**************************************************************************/
/*!
   @brief    Draws the name with the tag
   @param    distance - distance location in feet

   NOTE: To write this you must set the m_lineNum using setLineNum(uint16_t distance);
         Two default values.
         uint16_t cursorOffsetX, uint16_t cursorOffsetY have default values
 */
/**************************************************************************/
void Display::setDistance(uint16_t distance, uint16_t cursorOffsetX, uint16_t cursorOffsetY)
{
    uint16_t dist = distance;
    if (distance > MAXDISTANCEINFEET)
    {
        dist = 0;
    }
    char temp[MAXDISTANCECHARS];
    memset(temp,'\0',MAXDISTANCECHARS);
    sprintf(temp,"%d ft", dist);
    printf("\ntemp = %s\n",temp);
    if ((cursorOffsetX == 0) && (cursorOffsetY == 0))
    {
        centerTextCust(temp, MAXDISTANCECHARS, m_lineNum);
    }
    else
    {
        setLineNum(1);
        customTextposition(temp, MAXDISTANCECHARS, cursorOffsetX, cursorOffsetY);
    }
}

/**************************************************************************/
/*!
   @brief    Draws the name
   @param    namePtr - Name pointer to array
   @param    nameLen - Name Length

   NOTE: 1) You need to configure the line Number using setLineNum(uint8_t lineNum)
         2) uint16_t cursorOffsetX, uint16_t cursorOffsetY have default values
 */
/**************************************************************************/
void Display::setName(char *namePtr, uint16_t nameLen, uint16_t cursorOffsetX, uint16_t cursorOffsetY)
{
    if ((cursorOffsetX == 0) && (cursorOffsetY == 0))
    {
        centerTextCust(&namePtr[0], nameLen, m_lineNum);
    }
    else
    {
        customTextposition(&namePtr[0], nameLen, cursorOffsetX, cursorOffsetY);
    }
}

/**************************************************************************/
/*!
   @brief    sets the Cursor to the center and prints the text
   @param    nameLen - Name Length
   @param    lineNum - sets the lineNum

   NOTE: 1) the lineNum is local only and not saved, in m_lineNum
 */
/**************************************************************************/
void Display::centerTextCust(char *text, uint16_t nameLen, uint8_t lineNum)
{
    uint16_t strLen = g_Graphics.getStringLength(text);
    g_Graphics.setCursor((int16_t)(((resolution.width-strLen)/2)),(int16_t)((lineNum*fontHeight)));
    println(text,nameLen);
}


/**************************************************************************/
/*!
   @brief    sets the Cursor to the center with an offset and prints the text
   @param    nameLen - Name Length
   @param    lineNum - sets the lineNum
   @param    cursorOffsetX - X coordinate
   @param    cursorOffsetY - Y coordinate

 */
/**************************************************************************/
void Display::customTextposition(char *text, uint16_t nameLen, uint16_t cursorOffsetX, uint16_t cursorOffsetY)
{
    if (cursorOffsetX > resolution.width) { cursorOffsetX = 0; }
    if (cursorOffsetY > resolution.height) { cursorOffsetY = 0; }
    g_Graphics.setCursor((int16_t)(cursorOffsetX),(int16_t)(cursorOffsetY));
    println(text,nameLen);
}

/**************************************************************************/
/*!
   @brief    sets the Cursor offset for custom text locations
   @param    x - x axis
   @param    y - y axis

   NOTE: m_cursorOffsetX and m_cursorOffsetY are used right now for centerTextCust() which is set to the center, You may need to also check setCursor() in
   the graphics module and adjust that as well.
 */
/**************************************************************************/
void Display::setCursorOffset(uint16_t x, uint16_t y)
{
    m_cursorOffsetX = x;
    m_cursorOffsetY = y;
}

/**************************************************************************/
/*!
   @brief    sets the line number
   @param    lineNum - line number.

   NOTE: m_lineNum is used to give spacing between the text when the next text is on another line. this way they do not collide with each other if you are
   sing different font sizes.
 */
/**************************************************************************/
void Display::setLineNum(uint8_t lineNum)
{
    m_lineNum = lineNum;
}

/**************************************************************************/
/*!
   @brief    carriage return, line feed
   @param    None

   NOTE: ensure the setCursor(x,y) whcih is defined in the graphics module is set where you want it to be before writing
 */
/**************************************************************************/
size_t Display::printcrlf(void)
{
    uint8_t buf[2]= {'\r', '\n'};
    return  writeStream(buf, 2);
}

/**************************************************************************/
/*!
   @brief    Println helper function, prints a line of text
   @param    s - pointer to array to be sent
   @param    nameLen - size of the array

   NOTE: ensure the setCursor(x,y) whcih is defined in the graphics module is set where you want it to be before writing
 */
/**************************************************************************/
size_t Display::println(char *s, uint16_t nameLen)
{
    return print(s,nameLen) + printcrlf();
}

/**************************************************************************/
/*!
   @brief    Println helper function, prints a line of text
   @param    s - pointer to array to be sent
   @param    nameLen - size of the array

   NOTE: ensure the setCursor(x,y) whcih is defined in the graphics module is set where you want it to be before writing
 */
/**************************************************************************/
size_t Display::print(char *s, uint16_t nameLen)
{
    size_t count = 0;
    unsigned int index = 0;
    unsigned int len = nameLen;
    unsigned int nbytes = len;

    index += nbytes;
    len -= nbytes;

    count +=  writeStream((const uint8_t*) s, nbytes);
    return count;
}

/**************************************************************************/
/*!
   @brief    writes the text stream
   @param    buffer - pointer to array to be sent
   @param    size - size of the array

   NOTE: ensure the setCursor(x,y) whcih is defined in the graphics module is set where you want it to be before writing
 */
/**************************************************************************/
size_t Display::writeStream(const uint8_t *buffer, size_t size)
{
    size_t count = 0;
    while (size--)
    {
        count = count + 1;
        g_Graphics.write(*buffer++);
    }
    return count;
}

/**************************************************************************/
/*!
   @brief    draws the lynq label image
   @param    buffer - pointer to array to be sent
   @param    size - size of the array

   NOTE: 1) This functions clears the display buffer

 */
/**************************************************************************/
void Display::drawLynqLabel(uint16_t color)
{
    g_Graphics.clearRect();
    g_Graphics.drawBitmapImage(0, 0, lynqLabel, 128, 128, color);
}

/**************************************************************************/
/*!
   @brief    draws shapes available in Shape_t can be printed on screen
   @param    shape - enum of the shape object
   @param    color - based upon the invertDisplay but 0 for black and 1 for white.
   @param    argp - structure of the dimentions needed.

   NOTE: argp is a void* which means it has to be typecasted to the correct Structure

 */
/**************************************************************************/
void Display::drawShape(Shape_t shape, uint16_t color, void* argp)
{
    switch(shape)
    {
    case SHAPE_Point:
    {
        g_Graphics.drawPixel(((ShapePoint_t *)argp)->x,
                ((ShapePoint_t *)argp)->y,
                color);
        break;
    }
    case SHAPE_Line:
    {
        g_Graphics.drawLine(((ShapeLine_t *)argp)->start.x,
                ((ShapeLine_t *)argp)->start.y,
                ((ShapeLine_t *)argp)->end.x,
                ((ShapeLine_t *)argp)->end.y,
                color);
        break;
    }
    case SHAPE_Circle:
    {
        g_Graphics.drawCircle(((ShapeCircle_t *)argp)->center.x,
                ((ShapeCircle_t *)argp)->center.y,
                ((ShapeCircle_t *)argp)->radius,
                color);
        break;
    }
    case SHAPE_Triangle:
    {
        g_Graphics.fillTriangle(((ShapeTriangle_t *)argp)->first.x,
                ((ShapeTriangle_t *)argp)->first.y,
                ((ShapeTriangle_t *)argp)->second.x,
                ((ShapeTriangle_t *)argp)->second.y,
                ((ShapeTriangle_t *)argp)->third.x,
                ((ShapeTriangle_t *)argp)->third.y,
                color);
        break;
    }
    case SHAPE_Rectangle:
    {
        g_Graphics.drawRect(((ShapeRectangle_t *)argp)->start.x,
                ((ShapeRectangle_t *)argp)->start.y,
                ((ShapeRectangle_t *)argp)->width,
                ((ShapeRectangle_t *)argp)->height,
                color);
        break;
    }
    case SHAPE_FillRect:
    {
        g_Graphics.fillRect(((ShapeRectangle_t *)argp)->start.x,
                ((ShapeRectangle_t *)argp)->start.y,
                ((ShapeRectangle_t *)argp)->width,
                ((ShapeRectangle_t *)argp)->height,
                color);
        break;
    }
    case SHAPE_FillRoundRect:
    {
        g_Graphics.fillRoundRect(((ShapeRectangle_t *)argp)->start.x,
                ((ShapeRectangle_t *)argp)->start.y,
                ((ShapeRectangle_t *)argp)->width,
                ((ShapeRectangle_t *)argp)->height,6,
                color);
        break;
    }
    case SHAPE_Arc:
    {
        uint16_t angle_trig;
        uint16_t spreadAngle = ((ShapeArc_t *)argp)->arcLength/2;
        uint16_t heading = ((ShapeArc_t *)argp)->heading;
        // Convert heading into trig degrees.
        angle_trig = (360 + 90 - heading) % 360;

        uint16_t arcRadius = ((ShapeArc_t *)argp)->radius;

        for (uint16_t i = 1 ; i <= spreadAngle ; i++)
        {
            g_Graphics.drawPixel((icos(i+angle_trig)*arcRadius)+((ShapeArc_t *)argp)->x, (-isin(i+angle_trig)*arcRadius)+((ShapeArc_t *)argp)->y,0);
            g_Graphics.drawPixel((icos(i+angle_trig)*(arcRadius-1))+((ShapeArc_t *)argp)->x, (-isin(i+angle_trig)*(arcRadius-1))+((ShapeArc_t *)argp)->y,0);
            g_Graphics.drawPixel((icos(angle_trig-i)*arcRadius)+((ShapeArc_t *)argp)->x, (-isin(angle_trig-i)*arcRadius)+((ShapeArc_t *)argp)->y,0);
            g_Graphics.drawPixel((icos(angle_trig-i)*(arcRadius-1))+((ShapeArc_t *)argp)->x, (-isin(angle_trig-i)*(arcRadius-1))+((ShapeArc_t *)argp)->y,0);
        }


        break;
    }
    case SHAPE_Arrow:
    {
        int16_t x2=0;
        int16_t y2=0;
        int16_t edgeLength = 55 ;
        int16_t arrowArm = 40 ;
        int16_t osx = X_CENTER;
        int16_t osy = Y_CENTER;
        uint8_t arrowWidth = 25, rectWidth=10;

        int16_t headingAngle =  *((ShapeArrow_t *)argp);

        x2 = -icos(headingAngle) * edgeLength;
        y2 = isin(headingAngle) * edgeLength;

        // Arrow Head Point
        int16_t xt = -x2+osx;
        int16_t yt = -y2+osy;

        // Arrow Arms end points
        int16_t xt1 = (icos(headingAngle+arrowWidth) * arrowArm)+osx;
        int16_t yt1 = (-isin(headingAngle+arrowWidth) * arrowArm )+osy;
        int16_t xt2 = (icos(headingAngle-arrowWidth) * arrowArm)+osx;
        int16_t yt2 = (-isin(headingAngle-arrowWidth) * arrowArm )+osy;

        int16_t xt3 = (-icos(headingAngle+rectWidth) * arrowArm);
        int16_t yt3 = (isin(headingAngle+rectWidth) * arrowArm );
        int16_t xt4 = (-icos(headingAngle-rectWidth) * arrowArm);
        int16_t yt4 = (isin(headingAngle-rectWidth) * arrowArm );

        int16_t xrect1 = -xt3+osx;
        int16_t yrect1 = -yt3+osy;
        int16_t xrect2 = xt3+osx;
        int16_t yrect2 = yt3+osy;

        int16_t xrect3 = -xt4+osx;
        int16_t yrect3 = -yt4+osy;
        int16_t xrect4 = xt4+osx;
        int16_t yrect4 = yt4+osy;

        ShapeTriangle_t tri = {{xt,yt},{xt1,yt1},{xt2,yt2}};
        ShapeTriangle_t tri2 = {{xrect1,yrect1},{xrect2,yrect2},{xrect4,yrect4}};
        ShapeTriangle_t tri3 = {{xrect1,yrect1},{xrect3,yrect3},{xrect2,yrect2}};

        //The arrow consists of three triangles
        drawShape(SHAPE_Triangle, color, &tri);			//Arrowpoint
        drawShape(SHAPE_Triangle, color, &tri2);		//triangle left arm
        drawShape(SHAPE_Triangle, color, &tri3);		//triangle right arm

        break;
    }
    case SHAPE_BoldArc:
    {
        uint16_t angle_trig;
        uint16_t spreadAngle = ((ShapeArc_t *)argp)->arcLength/2;
        uint16_t heading = ((ShapeArc_t *)argp)->heading;
        // Convert heading into trig degrees.
        angle_trig = (360 + 90 - heading) % 360;

        uint16_t arcRadius = ((ShapeArc_t *)argp)->radius;

        uint8_t cirRadius = CIRCLE_RADIUS;

        if(spreadAngle == 0)
        {
            g_Graphics.fillCircle((icos(angle_trig)*arcRadius)+((ShapeArc_t *)argp)->x, (-isin(angle_trig)*arcRadius)+((ShapeArc_t *)argp)->y,cirRadius,0);
        }
        else
        {
            for (uint16_t i = 1 ; i <= spreadAngle ; i++)
            {
                g_Graphics.fillCircle((icos(i+angle_trig)*arcRadius)+((ShapeArc_t *)argp)->x, (-isin(i+angle_trig)*arcRadius)+((ShapeArc_t *)argp)->y,cirRadius,0);
                g_Graphics.fillCircle((icos(angle_trig-i)*arcRadius)+((ShapeArc_t *)argp)->x, (-isin(angle_trig-i)*arcRadius)+((ShapeArc_t *)argp)->y,cirRadius,0);
            }
        }
        break;
    }
    default:
    {
        break;
    }
    }
}

/**************************************************************************/
/*!
   @brief    gets the graphics location in the display buffer.
   @param    index - index of location

   NOTE: We use this for translation to a 2 dimentional array.

   TODO: Possibly move this to the graphics module.

 */
/**************************************************************************/
uint8_t*  Display::getDisplayPtr(uint16_t index)
{
    if ((index < 0) || (index > BUFFER_SIZE))
        return NULL;

    return g_Graphics.getDisplayPtr(index);
}

/**************************************************************************/
/*!
   @brief    sets the graphics location in the display buffer.
   @param    index - index of location
   @param    data - data value

   NOTE: We use this for translation from 2 dimensional LCD array to 1 dimensional Graphics array
 */
/**************************************************************************/
void  Display::setDisplayPtr(uint16_t index, uint8_t data)
{
    if ((index < 0) || (index > BUFFER_SIZE))
        return;

    g_Graphics.setDisplayPtr(index, data);
}

/**************************************************************************/
/*!
   @brief    To test out fastMath class
   @param    None
 */
/**************************************************************************/
void Display::testTrigFunct()
{
#ifdef DEFINE_PRINTF
    float x =  23.456;
    float y =  78.910;

    y = isin(x);

    printf("\n%f\n",y);

    y = icos(x);
    printf("\n%f\n",y);

    y = itan(x);
    printf("\n%f\n",y);

    y = fsin(y);
    printf("\n%f\n",y);

    y = fcos(y);
    printf("\n%f\n",y);

    y = FABS(y);
    printf("\n%f\n",y);

    y = atantwo(x,y);
    printf("\n%f\n",y);
#endif

}

/**************************************************************************/
/*!
   @brief    To test out shapes
   @param    None
 */
/**************************************************************************/
void Display::testShapes()
{
    g_Graphics.initGraphics();
    g_Graphics.setBuffer();
    testCharacters();
}

/**************************************************************************/
/*!
   @brief    To test out characters
   @param    None
 */
/**************************************************************************/
void Display::testCharacters()
{
    g_Graphics.initGraphics();
    g_Graphics.setBuffer();
    char ch = 'x';
    g_Graphics.write(ch);
    uint16_t size = 0;
    size = g_Graphics.getStringLength(&ch);
    printf("\n Size = %d",size);
}

/**************************************************************************/
/*!
   @brief    DISPLAY OBJECT
   @param    None
 */
/**************************************************************************/
Display g_Display = Display();
