/*
 * Copyright (c) 2019 Lynq TechnologiesinitGraphics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * is NOT PERMITTED without express written consent of Lynq Technologies.
 */

/*
 * Display.cpp
 *
 *  Created on: Nov 25, 2019
 *      Author: NKP
 */

#include "display.h"
#include "fast_math.h"
#include "lynq_label.h"
#include "graphics.h"

#define X_CENTER 115
#define Y_CENTER 111
#define CIRCLE_RADIUS 5

Display::Display(void) { }

/* Initialize the base parameters of the display variables here */
void Display::initDisplay(void)
{
  g_Graphics.initGraphics();
//  g_Graphics.setBuffer();
  resolution.width = SCREENDIMENSIONSWIDTH;/* number of width bits */
  resolution.height = SCREENDIMENSIONSHEIGHT;/* number of height bits */
  fontWidth = g_Graphics.getFontYAdvance();/* Widest character spacing */
  fontHeight = g_Graphics.getFontXAdvance();/* Tallest character spacing */
  setCursorOffset(0,0);
  setLineNum(0);
}


void Display::drawArc(uint16_t angle, uint16_t arcLength)
{
  ShapeArc_t tmpArc = {56,angle,arcLength};
  drawShape(SHAPE_BoldArc,0,&tmpArc);
}

/* Prints the time along with the user's name on the screen */
void Display::drawMainScreen(char *namePtr, uint16_t nameLen, Time_t* time)
{
  drawTime(time);
  drawNameWithTag(namePtr, nameLen);
}

/* Prints the arrow-graphic with group-member's name and distance on the screen */
void Display::drawMemberScreen(char *namePtr, uint16_t nameLenIn, uint16_t angleIn, uint16_t arcLengthIn, uint16_t distance)
{
  uint16_t nameLen = nameLenIn % MAXNAMELEN;
  uint16_t angle = angleIn % MAXANGLE;
  uint16_t arcLength = arcLengthIn % MAXARCLEN;

  drawArc(angle,arcLength);
  setDistance(distance);/* Set distance in Feet */
  setName(namePtr, nameLen);
}

void Display::drawTime(Time_t* timeIn)
{
  Time_t time;
  time.hours = timeIn->hours % HOURSINTMAX;
  time.mins = timeIn->mins % MINSINTMAX;
  time.seconds = timeIn->mins % SECONDSINTMAX;

  char temp[MAXTIMELENGTH];
  memset(temp,'\0',MAXTIMELENGTH);

  if(time.hours < 10)
    sprintf(temp+HOURSMARK,"0%d:",time.hours);
  else
    sprintf(temp+HOURSMARK,"%d:",time.hours);

#ifdef DEBUGLOW
  printf("\nTime = %s\n",temp);
#endif
  if(time.mins < 10)
    sprintf(temp+MINSMARK,"0%d:",time.mins);
  else
    sprintf(temp+MINSMARK,"%d:",time.mins);

#ifdef DEBUGLOW
  printf("\nTime = %s\n",temp);
#endif
  if(time.seconds < 10)
    sprintf(temp+SECSMARK,"0%d",time.seconds);
  else
    sprintf(temp+SECSMARK,"%d",time.seconds);

#ifdef DEBUG
  printf("\nTime = %s\n",temp);
#endif
  centerTextCust(temp,MAXTIMELENGTH,3);
}

void Display::drawNameWithTag(char *namePtr, uint16_t nameLen)
{
  char temp[MAXNAMETAGLEN];
  memset(temp,'\0',MAXTIMELENGTH);
  sprintf(temp,"Hi %s",namePtr);

  printf("\nName Tag = %s\n",temp);
  centerTextCust(temp,MAXTIMELENGTH,5);
}


void Display::setDistance(uint16_t distance)
{
  char temp[MAXDISTANCECHARS];
  memset(temp,'\0',MAXDISTANCECHARS);
  uint16_t dist = distance;
  if (distance > MAXDISTANCEINFEET)
  {
    dist = MAXDISTANCEINFEET;
  }
  sprintf(temp,"%d ft", dist);
  printf("\ntemp = %s\n",temp);
  centerTextCust(temp, MAXDISTANCECHARS, m_lineNum);
}

void Display::setName(char *namePtr, uint16_t nameLen)
{
  centerTextCust(namePtr, nameLen, m_lineNum);
}

void Display::centerTextCust(char *text, uint16_t nameLen, uint8_t lineNum)
{
  uint16_t strLen = g_Graphics.getStringLength(text);
  g_Graphics.setCursor((int16_t)(((resolution.width-strLen)/2) + m_cursorOffsetX),(int16_t)((lineNum*fontHeight) + m_cursorOffsetY));
  println(text,nameLen);
}

void Display::customTextposition(char *text, uint16_t nameLen, uint8_t lineNum)
{
  uint16_t strLen = g_Graphics.getStringLength(text);
  if (m_cursorOffsetX > resolution.width) { m_cursorOffsetX = 0; }
  if (m_cursorOffsetY > resolution.height) { m_cursorOffsetY = 0; }
  g_Graphics.setCursor((int16_t)(((resolution.width-strLen)/2)-m_cursorOffsetX),(int16_t)((lineNum*fontHeight)-m_cursorOffsetY));
  println(text,nameLen);
}

void Display::setCursorOffset(uint16_t x, uint16_t y)
{
	m_cursorOffsetX = x;
	m_cursorOffsetY = y;
}

void Display::setLineNum(uint8_t lineNum)
{
	m_lineNum = lineNum;
}

/* carriage return, line feed */
size_t Display::printcrlf(void)
{
  uint8_t buf[2]= {'\r', '\n'};
  return  writeStream(buf, 2);
}

/* Println helper function */
size_t Display::println(char *s, uint16_t nameLen)
{
  return print(s,nameLen) + printcrlf();
}

size_t Display::print(char *s, uint16_t nameLen)
{
  size_t count = 0;
  unsigned int index = 0;
  unsigned int len = nameLen;
  unsigned int nbytes = len;

  index += nbytes;
  len -= nbytes;

  count +=  writeStream((const uint8_t*) s, nbytes);
  return count;
}

size_t Display::writeStream(const uint8_t *buffer, size_t size)
{
  size_t count = 0;
  while (size--)
  {
    count = count + 1;
    g_Graphics.write(*buffer++);
  }
  return count;
}


void Display::drawLynqLabel(uint16_t color)
{
  g_Graphics.clearRect();
  g_Graphics.drawBitmapImage(0, 0, lynqLabel, 128, 128, color);
}

/* Shapes available in Shape_t can be printed on screen */
void Display::drawShape(Shape_t shape, uint16_t color, void* argp)
{
  switch(shape)
  {
	  case SHAPE_Point:
	  {
		g_Graphics.drawPixel(((ShapePoint_t *)argp)->x,
							 ((ShapePoint_t *)argp)->y,
							 color);
		break;
	  }
	  case SHAPE_Line:
	  {
		g_Graphics.drawLine(((ShapeLine_t *)argp)->start.x,
							((ShapeLine_t *)argp)->start.y,
							((ShapeLine_t *)argp)->end.x,
							((ShapeLine_t *)argp)->end.y,
							color);
		break;
	  }
	  case SHAPE_Circle:
	  {
		g_Graphics.drawCircle(((ShapeCircle_t *)argp)->center.x,
							  ((ShapeCircle_t *)argp)->center.y,
							  ((ShapeCircle_t *)argp)->radius,
							  color);
		break;
	  }
	  case SHAPE_Triangle:
	  {
		g_Graphics.fillTriangle(((ShapeTriangle_t *)argp)->first.x,
								((ShapeTriangle_t *)argp)->first.y,
								((ShapeTriangle_t *)argp)->second.x,
								((ShapeTriangle_t *)argp)->second.y,
								((ShapeTriangle_t *)argp)->third.x,
								((ShapeTriangle_t *)argp)->third.y,
								color);
		break;
	  }
	  case SHAPE_Rectangle:
	  {
		g_Graphics.drawRect(((ShapeRectangle_t *)argp)->start.x,
							((ShapeRectangle_t *)argp)->start.y,
							((ShapeRectangle_t *)argp)->width,
							((ShapeRectangle_t *)argp)->height,
							color);
		break;
	  }
	  case SHAPE_FillRect:
	  {
		g_Graphics.fillRect(((ShapeRectangle_t *)argp)->start.x,
							((ShapeRectangle_t *)argp)->start.y,
							((ShapeRectangle_t *)argp)->width,
							((ShapeRectangle_t *)argp)->height,
							color);
		break;
	  }
	  case SHAPE_FillRoundRect:
	  {
		g_Graphics.fillRoundRect(((ShapeRectangle_t *)argp)->start.x,
								 ((ShapeRectangle_t *)argp)->start.y,
								 ((ShapeRectangle_t *)argp)->width,
								 ((ShapeRectangle_t *)argp)->height,6,
								 color);
		break;
	  }
	  case SHAPE_Arc:
	  {
		uint16_t spreadAngle = ((ShapeArc_t *)argp)->arcLength/2;
		uint16_t heading = ((ShapeArc_t *)argp)->heading;

		uint16_t arcRadius = ((ShapeArc_t *)argp)->radius;

		for (uint16_t i = 1 ; i <= spreadAngle ; i++)
		{
		  g_Graphics.drawPixel((icos(i+heading)*arcRadius)+X_CENTER, (-isin(i+heading)*arcRadius)+Y_CENTER,0);
		  g_Graphics.drawPixel((icos(i+heading)*(arcRadius-1))+X_CENTER, (-isin(i+heading)*(arcRadius-1))+Y_CENTER,0);
		  g_Graphics.drawPixel((icos(heading-i)*arcRadius)+X_CENTER, (-isin(heading-i)*arcRadius)+Y_CENTER,0);
		  g_Graphics.drawPixel((icos(heading-i)*(arcRadius-1))+X_CENTER, (-isin(heading-i)*(arcRadius-1))+Y_CENTER,0);
		}


		break;
	  }
	  case SHAPE_Arrow:
	  {
		int16_t x2=0;
		int16_t y2=0;
		int16_t edgeLength = 55 ;
		int16_t arrowArm = 40 ;
		int16_t osx = X_CENTER;
		int16_t osy = Y_CENTER;
		uint8_t arrowWidth = 25, rectWidth=10;

		int16_t headingAngle =  *((ShapeArrow_t *)argp);

		x2 = -icos(headingAngle) * edgeLength;
		y2 = isin(headingAngle) * edgeLength;

		// Arrow Head Point
		int16_t xt = -x2+osx;
		int16_t yt = -y2+osy;

		// Arrow Arms end points
		int16_t xt1 = (icos(headingAngle+arrowWidth) * arrowArm)+osx;
		int16_t yt1 = (-isin(headingAngle+arrowWidth) * arrowArm )+osy;
		int16_t xt2 = (icos(headingAngle-arrowWidth) * arrowArm)+osx;
		int16_t yt2 = (-isin(headingAngle-arrowWidth) * arrowArm )+osy;

		int16_t xt3 = (-icos(headingAngle+rectWidth) * arrowArm);
		int16_t yt3 = (isin(headingAngle+rectWidth) * arrowArm );
		int16_t xt4 = (-icos(headingAngle-rectWidth) * arrowArm);
		int16_t yt4 = (isin(headingAngle-rectWidth) * arrowArm );

		int16_t xrect1 = -xt3+osx;
		int16_t yrect1 = -yt3+osy;
		int16_t xrect2 = xt3+osx;
		int16_t yrect2 = yt3+osy;

		int16_t xrect3 = -xt4+osx;
		int16_t yrect3 = -yt4+osy;
		int16_t xrect4 = xt4+osx;
		int16_t yrect4 = yt4+osy;

		ShapeTriangle_t tri = {{xt,yt},{xt1,yt1},{xt2,yt2}};
		ShapeTriangle_t tri2 = {{xrect1,yrect1},{xrect2,yrect2},{xrect4,yrect4}};
		ShapeTriangle_t tri3 = {{xrect1,yrect1},{xrect3,yrect3},{xrect2,yrect2}};

		drawShape(SHAPE_Triangle, color, &tri);
		drawShape(SHAPE_Triangle, color, &tri2);
		drawShape(SHAPE_Triangle, color, &tri3);

		break;
	  }
	  case SHAPE_BoldArc:
	  {
		uint16_t spreadAngle = ((ShapeArc_t *)argp)->arcLength/2;
		uint16_t heading = ((ShapeArc_t *)argp)->heading;

		uint16_t arcRadius = ((ShapeArc_t *)argp)->radius;

		uint8_t cirRadius = CIRCLE_RADIUS;

		if(spreadAngle == 0)
		{
		  g_Graphics.fillCircle((icos(heading)*arcRadius)+X_CENTER, (-isin(heading)*arcRadius)+Y_CENTER,cirRadius,0);
		}
		else
		{
		  for (uint16_t i = 1 ; i <= spreadAngle ; i++)
		  {
			g_Graphics.fillCircle((icos(i+heading)*arcRadius)+X_CENTER, (-isin(i+heading)*arcRadius)+Y_CENTER,cirRadius,0);
			g_Graphics.fillCircle((icos(heading-i)*arcRadius)+X_CENTER, (-isin(heading-i)*arcRadius)+Y_CENTER,cirRadius,0);
		  }
		}
		break;
	  }
	  default:
	  {
		break;
	  }
  }
}

uint8_t*  Display::getDisplayPtr(uint16_t index)
{
	if ((index < 0) || (index > BUFFER_SIZE))
		return NULL;

	return g_Graphics.getDisplayPtr(index);
}

/* To test out fastMath class */
void Display::testTrigFunct()
{
#ifdef DEFINE_PRINTF
  float x =  23.456;
  float y =  78.910;

  y = isin(x);

  printf("\n%f\n",y);

  y = icos(x);
  printf("\n%f\n",y);

  y = itan(x);
  printf("\n%f\n",y);

  y = fsin(y);
  printf("\n%f\n",y);

  y = fcos(y);
  printf("\n%f\n",y);

  y = FABS(y);
  printf("\n%f\n",y);

  y = atantwo(x,y);
  printf("\n%f\n",y);
#endif

}

/* For testing */
void Display::testShapes()
{
  g_Graphics.initGraphics();
  g_Graphics.setBuffer();
  testCharacters();
}

/* For testing */
void Display::testCharacters()
{
  g_Graphics.initGraphics();
  g_Graphics.setBuffer();
  char ch = 'x';
  g_Graphics.write(ch);
  uint16_t size = 0;
  size = g_Graphics.getStringLength(&ch);
  printf("\n Size = %d",size);
}

Display g_Display = Display();
