/**
  ******************************************************************************
  * @file         display_wrapper.cpp
  * @brief        Wrapper for C++ class definition.
  *
  *
  ******************************************************************************
  *
  * Copyright (c) 2019-2020 OTTO Engineering, Inc.
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without
  * modification, are prohibited.
  *
  ******************************************************************************
  */

#include "display.h"

extern "C" void LynqGraphics_initDisplayLibrary(void);

extern "C" void LynqGraphics_drawDisplayLynqLabel(uint16_t color);
extern "C" void LynqGraphics_drawShapeLine(uint16_t color, int16_t xStart, int16_t yStart, int16_t xEnd, int16_t yEnd);
extern "C" void LynqGraphics_drawShapePoint(uint16_t color, int16_t x, int16_t y);
extern "C" void LynqGraphics_drawShapeCircle(uint16_t color, int16_t x, int16_t y, uint16_t radius);
extern "C" void LynqGraphics_drawShapeTriangle(uint16_t color, int16_t firstX, int16_t firstY,
		                                          int16_t secondX, int16_t secondY,
		                                          int16_t thirdX, int16_t thirdY);
extern "C" void LynqGraphics_drawShapeRectangle(uint16_t color, int16_t x, int16_t y, int16_t width, int16_t height);
extern "C" void LynqGraphics_drawShapeFillRect(uint16_t color, int16_t x, int16_t y, int16_t width, int16_t height);
extern "C" void LynqGraphics_drawShapeFillRoundRect(uint16_t color, int16_t x, int16_t y, int16_t width, int16_t height);
extern "C" void LynqGraphics_drawShapeArc(uint16_t color, int16_t, int16_t, int16_t radius, int16_t heading, int16_t arcLength);
extern "C" void LynqGraphics_drawShapeArrow(uint16_t color, int16_t arrowHead);
extern "C" void LynqGraphics_drawShapeBoldArc(uint16_t color, int16_t, int16_t, int16_t radius, int16_t heading, int16_t arcLength);
extern "C" void LynqGraphics_drawTime(uint8_t hours, uint8_t mins, uint8_t seconds);
extern "C" void LynqGraphics_drawMemberScreen(char *namePtr, uint16_t nameLenIn, uint16_t angleIn, uint16_t arcLengthIn, uint16_t distance);
extern "C" void LynqGraphics_drawMainScreen(char *namePtr, uint16_t nameLen, uint8_t hours, uint8_t min, uint8_t sec);
extern "C" void LynqGraphics_drawArc(uint16_t angle, uint16_t arcLength);
extern "C" void LynqGraphics_drawNameWithTag(char *namePtr, uint16_t nameLen);

extern "C" size_t LynqGraphics_writeStream(const uint8_t *buffer, size_t size);

extern "C" size_t LynqGraphics_print(char *s, uint16_t nameLen);
extern "C" size_t LynqGraphics_println(char *s, uint16_t nameLen);
extern "C" size_t LynqGraphics_printcrlf(void);

extern "C" void LynqGraphics_centerTextCust(char *text, uint16_t nameLen, uint8_t lineNum);
extern "C" void LynqGraphics_customTextposition(char *text, uint16_t nameLen, uint16_t x, uint16_t y);
extern "C" void LynqGraphics_setName(char *namePtr, uint16_t nameLen);
extern "C" void LynqGraphics_setCoordName(char *namePtr, uint16_t nameLen, uint16_t cursorOffsetX, uint16_t cursorOffsetY); //Overloaded setName in Display.cpp
extern "C" void LynqGraphics_setDistance(uint16_t distance);
extern "C" void LynqGraphics_setCoordDistance(uint16_t distance, uint16_t cursorOffsetX, uint16_t cursorOffsetY); //Overloaded setDistance in Display.cpp
extern "C" void LynqGraphics_setCursorOffset(uint16_t x, uint16_t y);
extern "C" void LynqGraphics_setLineNum(uint8_t lineNum);
extern "C" void LynqGraphics_setDisplayPtr(uint16_t index, uint8_t data);

extern "C" uint8_t* LynqGraphics_getDisplayPtr(uint16_t index);

//***** typedefs defines *****//

//***** typedefs enums *****//

//***** typedefs structs *****//

//***** API Functions definition *****//
//****Remember "API" is used like "public" in C++*******//

void LynqGraphics_initDisplayLibrary(void)
{
	g_Display.initDisplay();
}

uint8_t* LynqGraphics_getDisplayPtr(uint16_t index)
{
	return g_Display.getDisplayPtr(index);
}

void LynqGraphics_setDisplayPtr(uint16_t index, uint8_t data)
{
	g_Display.setDisplayPtr(index, data);
}

void LynqGraphics_drawDisplayLynqLabel(uint16_t color)
{
	g_Display.drawLynqLabel(color);
}

void LynqGraphics_drawShapeLine(uint16_t color, int16_t xStart, int16_t yStart, int16_t xEnd, int16_t yEnd)
{
	struct _lineCoord
	{
		uint16_t xStart;
		uint16_t yStart;
		uint16_t xEnd;
		uint16_t yEnd;
	}lineCoord;

	lineCoord.xStart = xStart;
	lineCoord.yStart = yStart;
	lineCoord.xEnd = xEnd;
	lineCoord.yEnd = yEnd;

	g_Display.drawShape(Display::Shape_t::SHAPE_Line, color, (void*)&lineCoord);
}

void LynqGraphics_drawShapePoint(uint16_t color, int16_t x, int16_t y)
{
	struct _pointCoord
	{
		uint16_t x;
		uint16_t y;

	}pointCoord;

	pointCoord.x = x;
	pointCoord.y = y;

	g_Display.drawShape(Display::Shape_t::SHAPE_Point, color, (void*)&pointCoord);
}

void LynqGraphics_drawShapeCircle(uint16_t color, int16_t x, int16_t y, uint16_t radius)
{
	struct _circleCoord
	{
		uint16_t x;
		uint16_t y;
		uint16_t radius;

	}circleCoord;

	circleCoord.x = x;
	circleCoord.y = y;
	circleCoord.radius = radius;

	g_Display.drawShape(Display::Shape_t::SHAPE_Circle, color, (void*)&circleCoord);
}

void LynqGraphics_drawShapeTriangle(uint16_t color, int16_t firstX, int16_t firstY,
									   int16_t secondX, int16_t secondY,
									   int16_t thirdX, int16_t thirdY)
{
	struct _triangleCoord
	{
		uint16_t firstX;
		uint16_t firstY;
		uint16_t secondX;
		uint16_t secondY;
		uint16_t thirdX;
		uint16_t thirdY;

	}triangleCoord;

	triangleCoord.firstX = firstX;
	triangleCoord.firstY = firstY;
	triangleCoord.secondX = secondX;
	triangleCoord.secondY = secondY;
	triangleCoord.thirdX = thirdX;
	triangleCoord.thirdY = thirdY;

	g_Display.drawShape(Display::Shape_t::SHAPE_Triangle, color, (void*)&triangleCoord);
}

void LynqGraphics_drawShapeRectangle(uint16_t color, int16_t x, int16_t y, int16_t width, int16_t height)
{
	struct _rectangleCoord
	{
		uint16_t x;
		uint16_t y;
		uint16_t width;
		uint16_t height;

	}rectangleCoord;

	rectangleCoord.x = x;
	rectangleCoord.y = y;
	rectangleCoord.width = width;
	rectangleCoord.height = height;

	g_Display.drawShape(Display::Shape_t::SHAPE_Rectangle, color, (void*)&rectangleCoord);
}

void LynqGraphics_drawShapeFillRect(uint16_t color, int16_t x, int16_t y, int16_t width, int16_t height)
{
	struct _fillRectCoord
	{
		uint16_t x;
		uint16_t y;
		uint16_t width;
		uint16_t height;

	}fillRectCoord;

	fillRectCoord.x = x;
	fillRectCoord.y = y;
	fillRectCoord.width = width;
	fillRectCoord.height = height;

	g_Display.drawShape(Display::Shape_t::SHAPE_FillRect, color, (void*)&fillRectCoord);
}

void LynqGraphics_drawShapeFillRoundRect(uint16_t color, int16_t x, int16_t y, int16_t width, int16_t height)
{
	struct _fillRoundRectCoord
	{
		uint16_t x;
		uint16_t y;
		uint16_t width;
		uint16_t height;

	}fillRoundRectCoord;

	fillRoundRectCoord.x = x;
	fillRoundRectCoord.y = y;
	fillRoundRectCoord.width = width;
	fillRoundRectCoord.height = height;

	g_Display.drawShape(Display::Shape_t::SHAPE_FillRoundRect, color, (void*)&fillRoundRectCoord);
}

void LynqGraphics_drawShapeArc(uint16_t color,  int16_t x, int16_t y, int16_t radius, int16_t heading, int16_t arcLength)
{
	struct _arcLengthCoord
	{
		uint16_t radius;
		uint16_t heading;
		uint16_t arcLength;
		uint16_t x;
		uint16_t y;

	}arcLengthCoord;

	arcLengthCoord.radius = radius;
	arcLengthCoord.heading = heading;
	arcLengthCoord.arcLength = arcLength;

	g_Display.drawShape(Display::Shape_t::SHAPE_Arc, color, (void*)&arcLengthCoord);
}

void LynqGraphics_drawShapeArrow(uint16_t color, int16_t arrowHead)
{
	struct _arrowCoord
	{
		uint16_t arrowHead;

	}arrowCoord;

	arrowCoord.arrowHead = arrowHead;

	g_Display.drawShape(Display::Shape_t::SHAPE_Arrow, color, (void*)&arrowCoord);
}

void LynqGraphics_drawShapeBoldArc(uint16_t color, int16_t x, int16_t y, int16_t radius, int16_t heading, int16_t arcLength)
{
	struct _boldArcCoord
	{
		uint16_t radius;
		uint16_t heading;
		uint16_t arcLength;
		uint16_t x;
		uint16_t y;

	}boldArcCoord;

	boldArcCoord.radius = radius;
    boldArcCoord.heading = heading;
    boldArcCoord.arcLength = arcLength;
    boldArcCoord.x = x;
    boldArcCoord.y = y;

	g_Display.drawShape(Display::Shape_t::SHAPE_BoldArc, color, (void*)&boldArcCoord);
}

size_t  LynqGraphics_writeStream(const uint8_t *buffer, size_t size)
{
	return g_Display.writeStream(buffer, size);
}

size_t LynqGraphics_print(char *s, uint16_t nameLen)
{
	return g_Display.print(s, nameLen);
}

size_t LynqGraphics_println(char *s, uint16_t nameLen)
{
	return g_Display.println(s, nameLen);
}

size_t LynqGraphics_printcrlf(void)
{
	return g_Display.printcrlf();
}

void LynqGraphics_centerTextCust(char *text, uint16_t nameLen, uint8_t lineNum)
{
	g_Display.centerTextCust(text, nameLen, lineNum);
}

void LynqGraphics_setName(char *namePtr, uint16_t nameLen)
{
	g_Display.setName(namePtr, nameLen);
}

void LynqGraphics_setCoordName(char *namePtr, uint16_t nameLen, uint16_t cursorOffsetX, uint16_t cursorOffsetY)
{
	g_Display.setName(namePtr, nameLen, cursorOffsetX, cursorOffsetY);
}

void LynqGraphics_setDistance(uint16_t distance)
{
	g_Display.setDistance(distance);
}

void LynqGraphics_setCoordDistance(uint16_t distance, uint16_t cursorOffsetX, uint16_t cursorOffsetY)
{
	g_Display.setDistance(distance, cursorOffsetX, cursorOffsetY);
}

void LynqGraphics_drawNameWithTag(char *namePtr, uint16_t nameLen)
{
	g_Display.drawNameWithTag(namePtr, nameLen);
}

void LynqGraphics_drawTime(uint8_t hours, uint8_t mins, uint8_t seconds)
{
	Display::Time_t timeIn;
	timeIn.hours = hours;
	timeIn.mins = mins;
	timeIn.seconds = seconds;

	g_Display.drawTime(&timeIn);
}

void LynqGraphics_drawMemberScreen(char *namePtr, uint16_t nameLenIn, uint16_t angleIn, uint16_t arcLengthIn, uint16_t distance)
{
	g_Display.drawMemberScreen(namePtr, nameLenIn, angleIn, arcLengthIn, distance);
}

void LynqGraphics_drawMainScreen(char *namePtr, uint16_t nameLen, uint8_t hours, uint8_t mins, uint8_t sec)
{
	Display::Time_t time;
	time.hours = hours;
	time.mins = mins;
	time.seconds = sec;

	g_Display.drawMainScreen(namePtr, nameLen, &time);
}

void LynqGraphics_drawArc(uint16_t angle, uint16_t arcLength)
{
	g_Display.drawArc(angle, arcLength);
}

void LynqGraphics_customTextposition(char *text, uint16_t nameLen, uint16_t x, uint16_t y)
{
	g_Display.customTextposition(text, nameLen, x, y);
}

void LynqGraphics_setCursorOffset(uint16_t x, uint16_t y)
{
	g_Display.setCursorOffset(x, y);
}

void LynqGraphics_setLineNum(uint8_t lineNum)
{
	g_Display.setLineNum(lineNum);
}
