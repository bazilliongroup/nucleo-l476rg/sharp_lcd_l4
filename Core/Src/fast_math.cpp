/*
 * Copyright (c) 2019 Lynq Technologies
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * is NOT PERMITTED without express written consent of Lynq Technologies.
 */

/*
 * FastMath.cpp
 *
 *  Created on: Nov 25, 2019
 *      Author: NKP
 */

#include "fast_math.h"


/* Calcluates sin(Theta) */
float FastMathClass::isin(long x)
{
  bool pos = true;  /* positive - keeps an eye on the sign. */

  if (x < 0)
  {
    x = -x;
    pos = !pos;
  }

  if (x >= 360) x %= 360;

  if (x > 180)
  {
    x -= 180;
    pos = !pos;
  }

  if (x > 90) x = 180 - x;

#ifdef TABLE_PERCISION_16_BIT
  if (pos) return iSinTable16[x] * 0.0000152590219; // = /65535.0
  return isinTable16[x] * -0.0000152590219 ;
#else
  if (pos) return iSinTable8[x] * 0.003921568627; // = /255.0
  return iSinTable8[x] * -0.003921568627 ;
#endif
}

/* Calcluates cos(Theta) */
float FastMathClass::icos(long x)
{
  return isin(x+90);
}

/* Calcluates tan(Theta) */
float FastMathClass::itan(long x)
{
  return isin(x) / icos(x);
}

float FastMathClass::fsin(float d)
{
  float a = isin(d);
  float b = isin(d+1);
  return (a + (d- (int)d) * (b-a));
}

float FastMathClass::fcos(float d)
{
  float a = isin(d+90);
  float b = isin(d+91);
  return a + (d-(int)(d)) * (b-a);
}

double FastMathClass::FABS(float f)
{
  double temp = fabs(f);
  return temp;
}

float FastMathClass::atantwo(float y, float x)
{
  const float n1 = 0.97239411f;
  const float n2 = -0.19194795f;
  float result = 0.0f;
  if (x != 0.0f)
  {
    const union
	{
      float flVal;
      uint32_t nVal;

    } tYSign = { y };

    const union
	{
      float flVal;
      uint32_t nVal;

    } tXSign = { x };

    if (fabsf(x) >= fabsf(y))
    {
      union
	  {
        float flVal;
        uint32_t nVal;

      } tOffset = { PI };

      /* Add or subtract PI based on y's sign. */
      tOffset.nVal |= tYSign.nVal & 0x80000000u;

      /* No offset if x is positive, so multiply by 0 or based on x's sign. */
      tOffset.nVal *= tXSign.nVal >> 31;
      result = tOffset.flVal;
      const float z = y / x;
      result += (n1 + n2 * z * z) * z;
    }
    else	/* Use atan(y/x) = pi/2 - atan(x/y) if |y/x| > 1. */
    {
      union
	  {
        float flVal;
        uint32_t nVal;

      } tOffset = { PI/2 };

      /* Add or subtract PI/2 based on y's sign. */
      tOffset.nVal |= tYSign.nVal & 0x80000000u;
      result = tOffset.flVal;
      const float z = x / y;
      result -= (n1 + n2 * z * z) * z;
    }
  }
  else if (y > 0.0f)
  {
    result = PI/2;
  }
  else if (y < 0.0f)
  {
    result = -PI/2;
  }
  return result;
}
