
#include "graphics.h"
#include "silom12.h"

#if !defined(__INT_MAX__) || (__INT_MAX__ > 0xFFFF)
#define pgm_read_pointer(addr) ((void *)pgm_read_dword(addr))
#else
#define pgm_read_pointer(addr) ((void *)pgm_read_word(addr))
#endif

#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif

#ifndef _swap_int16_t
#define _swap_int16_t(a, b) { int16_t t = a; a = b; b = t; }
#endif

const uint8_t set[] = {  1,  2,  4,  8,  16,  32,  64,  128 };
const uint8_t clr[] = { 0xFE, 0xFD, 0xFB, 0xF7, 0xEF, 0xDF,0xBF,0x7F };


/**************************************************************************/
/*!
    @brief Initializes the graphics dimentions, font and cursors
    @param  None
*/
/**************************************************************************/
void Graphics::initGraphics()
{
  /* TODO: add #defines to this */

  m_invertDisplayFlag = true;
  m_widthSize = SCREENDIMENSIONSWIDTH;
  m_heightSize = SCREENDIMENSIONSHEIGHT;
  setCursor(0, 0);
  setBuffer();

  /* Initialize the basic font structure */
  setFont(&Silom12pt7b);
  setCursor(20, 50);
  setTextSize(1);
}

/**************************************************************************/
/*!
    @brief Set the font to display when print()ing, either custom or default
    @param  f  The GFXfont object, if NULL use built in 6x8 font
*/
/**************************************************************************/
void Graphics::setFont(const GFX_FONT *f)
{
  if(f)
  {
    if(!m_gfxFontPtr)
    {
      m_cursorY += 6;
    }
  }
  else if(m_gfxFontPtr)
  {
    // Move cursor pos up 6 pixels so it's at top-left of char.
    m_cursorY -= 6;
  }
  m_gfxFontPtr = (GFX_FONT *)f;
}

/**************************************************************************/
/*!
   @brief      Draw a RAM-resident 1-bit image at the specified (x,y) position, using the specified foreground (for set bits) and background (unset bits) colors.
    @param    x   Top left corner x coordinate
    @param    y   Top left corner y coordinate
    @param    bitmap  byte array with monochrome bitmap
    @param    w   Width of bitmap in pixels
    @param    h   Hieght of bitmap in pixels
    @param    color 0 - white 1 - black
*/
/**************************************************************************/
void Graphics::drawBitmap(int16_t x, int16_t y, const uint8_t *bitmap, int16_t w, int16_t h, uint16_t color)
{
  int16_t i;
  int16_t j;
  int16_t byteWidth = (w + 7) / 8;
  uint8_t byte = 0;

  for(j=0; j<h; j++)
  {
    for(i=0; i<w; i++)
    {
      if(i & 7)
        byte <<= 1;
      else
        byte   = pgm_read_byte(bitmap + j * byteWidth + i / 8);
      if(byte & 0x80)
        drawPixel(x+i, y+j, color);
    }
  }
}

/**************************************************************************/
/*!
   @brief    Draw or clear a pixel
    @param   x   x coordinate
    @param   y   y coordinate
   @param    color 0 - white 1 - black
*/
/**************************************************************************/
void Graphics::drawPixel(int16_t x, int16_t y, uint16_t color)
{

  if((x < 0) || (x >= m_widthSize) || (y < 0) || (y >= m_heightSize)) return;

//  _swap_int16_t(x, y);
//  x = SCREENDIMENSIONSWIDTH  - 1 - x;

#ifdef DEBUGLOW
  printf("x=%d,y=%d ",x,y);
#endif

  color = getColor(color);

#ifdef DEBUGLOW
  printf("%x\n",pgm_read_byte(&clr[x & 7]));
#endif

  uint16_t byte = ((y*SCREENDIMENSIONSWIDTH + x) / 8);

  if(color)
  {
    m_displayBuffer[byte] |= pgm_read_byte(&set[x & 7]);
  }
  else
  {
    m_displayBuffer[byte] &= pgm_read_byte(&clr[x & 7]);
  }
#ifdef DEBUGLOW
  printf("\ndisplayBuffer=%d",m_displayBuffer[(y*SCREENDIMENSIONSWIDTH + x) / 8]);
#endif
}

/**************************************************************************/
/*!
    @brief  Print one byte/character of data, used to support print()
    @param  c  The 8-bit ascii character to write
*/
/**************************************************************************/
void Graphics::write(uint8_t c)
{

  if(!m_gfxFontPtr)
  {
    /* TODO: Set Up default font */
  }
  else
  {
    // Custom font

    if(c == '\n')
    {
      m_cursorX  = 0;
      m_cursorY += (int16_t)m_textSize *(uint8_t)pgm_read_byte(&m_gfxFontPtr->yAdvance);
    }
    else if(c != '\r')
    {
      uint8_t first = pgm_read_byte(&m_gfxFontPtr->first);
      if((c >= first) && (c <= (uint8_t)pgm_read_byte(&m_gfxFontPtr->last)))
      {
        uint8_t   c2    = c - pgm_read_byte(&m_gfxFontPtr->first);

        GFX_GLYPH *glyph = &(((GFX_GLYPH *)pgm_read_pointer(&m_gfxFontPtr->glyph))[c2]);

        uint8_t   w     = pgm_read_byte(&glyph->width),
                  h     = pgm_read_byte(&glyph->height);

        if((w > 0) && (h > 0))
        {
          // Is there an associated bitmap?
          int16_t xo = (int8_t)pgm_read_byte(&glyph->xOffset); // sic

          if(m_wrap && ((m_cursorX + m_textSize * (xo + w)) >= m_widthSize))
          {
            // Drawing character would go off right edge; m_wrap to new line
            m_cursorX  = 0;
            m_cursorY += (int16_t)m_textSize * (uint8_t)pgm_read_byte(&m_gfxFontPtr->yAdvance);
          }
          drawChar(m_cursorX, m_cursorY, c, m_textColor, m_textBgColor, m_textSize);
        }
        m_cursorX += pgm_read_byte(&glyph->xAdvance) * (int16_t)m_textSize;
      }
    }
  }
}

/**************************************************************************/
/*!
   @brief   Draw a single character
    @param    x   Bottom left corner x coordinate
    @param    y   Bottom left corner y coordinate
    @param    c   The 8-bit font-indexed character (likely ascii)
    @param    color 0 - white 1 - black
    @param    bg 16-bit 5-6-5 Color to fill background with (if same as color, no background)
    @param    size  Font magnification level, 1 is 'original' size
*/
/**************************************************************************/
void Graphics::drawChar(int16_t x, int16_t y, unsigned char c, uint16_t color, uint16_t bg, uint8_t size)
{
  if(!m_gfxFontPtr)
  {
    /* TODO: Set Up default font */
  }
  else	// Custom font
  {

	// Character is assumed previously filtered by write() to eliminate
	// newlines, returns, non-printable characters, etc.  Calling
	// drawChar() directly with 'bad' characters of font may cause mayhem!

    c -= pgm_read_byte(&m_gfxFontPtr->first);
    GFX_GLYPH *glyph  = &(((GFX_GLYPH *)pgm_read_pointer(&m_gfxFontPtr->glyph))[c]);
    uint8_t  *bitmap = (uint8_t *)pgm_read_pointer(&m_gfxFontPtr->bitmap);

    uint16_t bo = pgm_read_word(&glyph->bitmapOffset);
    uint8_t  w  = pgm_read_byte(&glyph->width),
             h  = pgm_read_byte(&glyph->height);
    int8_t   xo = pgm_read_byte(&glyph->xOffset),
             yo = pgm_read_byte(&glyph->yOffset);
    uint8_t  xx, yy, bits = 0, bit = 0;
    int16_t  xo16 = 0, yo16 = 0;

    if(size > 1)
    {
      xo16 = xo;
      yo16 = yo;
    }

    for(yy=0; yy<h; yy++)
    {
      for(xx=0; xx<w; xx++)
      {
        if(!(bit++ & 7))
        {
          bits = pgm_read_byte(&bitmap[bo++]);
        }
        if(bits & 0x80)
        {
          if(size == 1)
          {
            drawPixel(x+xo+xx, y+yo+yy, color);
          }
          else
          {
            fillRect(x+(xo16+xx)*size, y+(yo16+yy)*size, size, size, color);
          }
        }
        bits <<= 1;
      }
    }
  }
}

/**************************************************************************/
/*!
    @brief
    @param
*/
/**************************************************************************/
uint16_t Graphics::getStringLength(char * str)
{
  uint8_t c; // Current character
  uint8_t len = 0;
  uint8_t x = 0, y = 0 ;
  if(m_gfxFontPtr)
  {
    // Custom Font
    GFX_GLYPH *glyph;
    uint8_t   first = pgm_read_byte(&m_gfxFontPtr->first),
              last  = pgm_read_byte(&m_gfxFontPtr->last),gw, gh, xa;
    int8_t  xo, yo;
    int16_t minx = m_widthSize, miny = m_heightSize, maxx = -1, maxy = -1,
            gx1, gy1, gx2, gy2, ts = (int16_t)m_textSize,
                                ya = ts * (uint8_t)pgm_read_byte(&m_gfxFontPtr->yAdvance);

    while((c = *str++))
    {
      if(c != '\n')
      {
        // Not a newline
        if(c != '\r')
        {
          // Not a carriage return, is normal char

          if((c >= first) && (c <= last))
          {
            // Char present in current font
            c    -= first;
            glyph = &(((GFX_GLYPH *)pgm_read_pointer(&m_gfxFontPtr->glyph))[c]);
            gw    = pgm_read_byte(&glyph->width);
            gh    = pgm_read_byte(&glyph->height);
            xa    = pgm_read_byte(&glyph->xAdvance);
            xo    = pgm_read_byte(&glyph->xOffset);
            yo    = pgm_read_byte(&glyph->yOffset);

            gx1 = x   + xo * ts;   // Width Added to gx1
            gy1 = y   + yo * ts;   // Height Added to gy1
            gx2 = gx1 + gw * ts - 1;
            gy2 = gy1 + gh * ts - 1;
            if(gx1 < minx) minx = gx1;  // If it is less Than 128
            if(gy1 < miny) miny = gy1;  // If It is Less tHan 128
            if(gx2 > maxx) maxx = gx2;  // iF it is More THan 0
            if(gy2 > maxy) maxy = gy2;  // If it is MOre than 0
            x += xa * ts;
          }
        } // Carriage return = do nothing
      }
      else
      {
        // Newline
        x  = 0;  // Reset x
        y += ya; // Advance y by 1 line
      }
    }

    if(maxx >= minx)
    	len  = maxx - minx + 1;
  }
  else
  {
    /* TODO: Set up Default font */

  }
  return len;
}

/**************************************************************************/
/*!
    @brief Take the widest character attributes
    @param
*/
/**************************************************************************/
uint8_t Graphics::getFontXAdvance()
{
  uint8_t xa ;
  GFX_GLYPH * glyph;

  if(m_gfxFontPtr)
  {
    glyph = &(((GFX_GLYPH *)pgm_read_pointer(&m_gfxFontPtr->glyph))[6]);/* Choosing character '%' */
    xa = pgm_read_byte(&glyph->width);
  }
  else
  {
    xa = 6;
  }
  return xa;
}

/**************************************************************************/
/*!
    @brief Take the Tallest character attributes
    @param
*/
/**************************************************************************/
uint8_t Graphics::getFontYAdvance()
{
  uint8_t ya;
  GFX_GLYPH * glyph;

  if(m_gfxFontPtr)
  {
    glyph = &(((GFX_GLYPH *)pgm_read_pointer(&m_gfxFontPtr->glyph))[45]);/* Choosing character 'L' */
    ya = 2 + pgm_read_byte(&glyph->height);
  }
  else
  {
    ya = 8;
  }
  return ya;
}


/**************************************************************************/
/*!
   @brief    Draw a circle outline
    @param    x0   Center-point x coordinate
    @param    y0   Center-point y coordinate
    @param    r   Radius of circle
    @param    color 0 - white 1 - black
*/
/**************************************************************************/
void Graphics::drawCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color)
{
  int16_t f = 1 - r;
  int16_t ddF_x = 1;
  int16_t ddF_y = -2 * r;
  int16_t x = 0;
  int16_t y = r;

  drawPixel(x0, y0+r, color);
  drawPixel(x0, y0-r, color);
  drawPixel(x0+r, y0, color);
  drawPixel(x0-r, y0, color);

  while (x<y)
  {
    if (f >= 0)
    {
      y--;
      ddF_y += 2;
      f += ddF_y;
    }
    x++;
    ddF_x += 2;
    f += ddF_x;

    drawPixel(x0 + x, y0 + y, color);
    drawPixel(x0 - x, y0 + y, color);
    drawPixel(x0 + x, y0 - y, color);
    drawPixel(x0 - x, y0 - y, color);
    drawPixel(x0 + y, y0 + x, color);
    drawPixel(x0 - y, y0 + x, color);
    drawPixel(x0 + y, y0 - x, color);
    drawPixel(x0 - y, y0 - x, color);
  }
}

/**************************************************************************/
/*!
   @brief    Quarter-circle drawer, used to do circles and roundrects
    @param    x0   Center-point x coordinate
    @param    y0   Center-point y coordinate
    @param    r   Radius of circle
    @param    cornername  Mask bit #1 or bit #2 to indicate which quarters of the circle we're doing
    @param    color 0 - white 1 - black
*/
/**************************************************************************/
void Graphics::drawCircleHelper( int16_t x0, int16_t y0, int16_t r, uint8_t cornername, uint16_t color)
{
  int16_t f     = 1 - r;
  int16_t ddF_x = 1;
  int16_t ddF_y = -2 * r;
  int16_t x     = 0;
  int16_t y     = r;

  while (x<y)
  {
    if (f >= 0)
    {
      y--;
      ddF_y += 2;
      f     += ddF_y;
    }
    x++;
    ddF_x += 2;
    f+= ddF_x;
    if (cornername & 0x4)
    {
      drawPixel(x0 + x, y0 + y, color);
      drawPixel(x0 + y, y0 + x, color);
    }
    if (cornername & 0x2)
    {
      drawPixel(x0 + x, y0 - y, color);
      drawPixel(x0 + y, y0 - x, color);
    }
    if (cornername & 0x8)
    {
      drawPixel(x0 - y, y0 + x, color);
      drawPixel(x0 - x, y0 + y, color);
    }
    if (cornername & 0x1)
    {
      drawPixel(x0 - y, y0 - x, color);
      drawPixel(x0 - x, y0 - y, color);
    }
  }
}

/**************************************************************************/
/*!
   @brief    Draw a circle with filled color
    @param    x0   Center-point x coordinate
    @param    y0   Center-point y coordinate
    @param    r   Radius of circle
    @param    color 0 - white 1 - black
*/
/**************************************************************************/
void Graphics::fillCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color)
{
  drawFastVLine(x0, y0-r, 2*r+1, color);
  fillCircleHelper(x0, y0, r, 3, 0, color);
}

/**************************************************************************/
/*!
   @brief    Quarter-circle drawer with fill, used to do circles and roundrects
    @param    x0   Center-point x coordinate
    @param    y0   Center-point y coordinate
    @param    r   Radius of circle
    @param    cornername  Mask bit #1 or bit #2 to indicate which quarters of the circle we're doing
    @param    delta  Offset from center-point, used for round-rects
    @param    color 0 - white 1 - black
*/
/**************************************************************************/
void Graphics::fillCircleHelper(int16_t x0, int16_t y0, int16_t r,
                                uint8_t cornername, int16_t delta, uint16_t color)
{

  int16_t f     = 1 - r;
  int16_t ddF_x = 1;
  int16_t ddF_y = -2 * r;
  int16_t x     = 0;
  int16_t y     = r;
  while (x<y)
  {
    if (f >= 0)
    {
      y--;
      ddF_y += 2;
      f+= ddF_y;
    }
    x++;
    ddF_x += 2;
    f+= ddF_x;

    if (cornername & 0x1)
    {
      drawFastVLine(x0+x, y0-y, 2*y+1+delta, color);
      drawFastVLine(x0+y, y0-x, 2*x+1+delta, color);
    }
    if (cornername & 0x2)
    {
      drawFastVLine(x0-x, y0-y, 2*y+1+delta, color);
      drawFastVLine(x0-y, y0-x, 2*x+1+delta, color);
    }
  }
}

/**************************************************************************/
/*!
   @brief    Draw a line using Bresenham's algorithm
    @param    x0  Start point x coordinate
    @param    y0  Start point y coordinate
    @param    x1  End point x coordinate
    @param    y1  End point y coordinate
    @param    color 0 - white 1 - black
*/
/**************************************************************************/
void Graphics::drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color)
{
#ifdef DEBUGLOW
  printf("x0=%d,y0=%d,x1=%d,y1=%d,color=%d\n",x0,y0,x1,y1,color);
#endif
  int16_t steep = abs(y1 - y0) > abs(x1 - x0);
  if (steep)
  {
    _swap_int16_t(x0, y0);
    _swap_int16_t(x1, y1);
  }

  if (x0 > x1)
  {
    _swap_int16_t(x0, x1);
    _swap_int16_t(y0, y1);
  }

  int16_t dx, dy;
  dx = x1 - x0;
  dy = abs(y1 - y0);

#ifdef DEBUGLOW
  printf("x0=%d,y0=%d,x1=%d,y1=%d,dx=%d,dy=%d\n",x0,y0,x1,y1,dx,dy);
#endif

  int16_t err = dx / 2;
  int16_t ystep;

  if (y0 < y1)
  {
    ystep = 1;
  }
  else
  {
    ystep = -1;
  }

  for (; x0<=x1; x0++)
  {
    if (steep)
    {
      drawPixel(y0, x0, color);
    }
    else
    {
      drawPixel(x0, y0, color);
    }
    err -= dy;

    if (err < 0)
    {
      y0 += ystep;
      err += dx;
    }
  }
}

/**************************************************************************/
/*!
   @brief   Draw a rectangle with no fill color
    @param    x   Top left corner x coordinate
    @param    y   Top left corner y coordinate
    @param    w   Width in pixels
    @param    h   Height in pixels
    @param    color 0 - white 1 - black
*/
/**************************************************************************/
void Graphics::drawRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color)
{
  drawFastHLine(x, y, w, color);
  drawFastHLine(x, y+h-1, w, color);
  drawFastVLine(x, y, h, color);
  drawFastVLine(x+w-1, y, h, color);
}

/**************************************************************************/
/*!
   @brief    Draw a perfectly vertical line (this is often optimized in a subclass!)
    @param    x   Top-most x coordinate
    @param    y   Top-most y coordinate
    @param    h   Height in pixels
   @param    color 0 - white 1 - black
*/
/**************************************************************************/
void Graphics::drawFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color)
{
  // Update in subclasses if desired!
  drawLine(x, y, x, y+h-1, color);
}

/**************************************************************************/
/*!
   @brief    Draw a perfectly horizontal line (this is often optimized in a subclass!)
    @param    x   Left-most x coordinate
    @param    y   Left-most y coordinate
    @param    w   Width in pixels
   @param    color 0 - white 1 - black
*/
/**************************************************************************/
void Graphics::drawFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color)
{
  // Update in subclasses if desired!
  drawLine(x, y, x+w-1, y, color);
}

/**************************************************************************/
/*!
   @brief    Fill a rectangle completely with one color. Update in subclasses if desired!
    @param    x   Top left corner x coordinate
    @param    y   Top left corner y coordinate
    @param    w   Width in pixels
    @param    h   Height in pixels
   @param    color 0 - white 1 - black
*/
/**************************************************************************/
void Graphics::fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color)
{
  // Update in subclasses if desired!
  for (int16_t i=x; i<x+w; i++)
  {
    drawFastVLine(i, y, h, color);
  }
}

/**************************************************************************/
/*!
   @brief    Fill a rectangle completely with one color. Update in subclasses if desired!
    @param    x   Top left corner x coordinate
    @param    y   Top left corner y coordinate
    @param    w   Width in pixels
    @param    h   Height in pixels
   @param    color 0 - white 1 - black
*/
/**************************************************************************/
void Graphics::fillScreen(uint16_t color)
{
  color = getColor(color);
  fillRect(0, 0, m_widthSize, m_heightSize, color);
}

/**************************************************************************/
/*!
   @brief   Draw a rounded rectangle with no fill color
    @param    x   Top left corner x coordinate
    @param    y   Top left corner y coordinate
    @param    w   Width in pixels
    @param    h   Height in pixels
    @param    r   Radius of corner rounding
    @param    color 0 - white 1 - black
*/
/**************************************************************************/
void Graphics::drawRoundRect(int16_t x, int16_t y, int16_t w, int16_t h, int16_t r, uint16_t color)
{
  // color = getColor(color);
  // smarter version
  drawFastHLine(x+r, y, w-2*r, color);       // Top
  drawFastHLine(x+r, y+h-1, w-2*r, color);   // Bottom
  drawFastVLine(x, y+r, h-2*r, color);       // Left
  drawFastVLine(x+w-1, y+r, h-2*r, color);   // Right
  // draw four corners
  drawCircleHelper(x+r, y+r, r, 1, color);
  drawCircleHelper(x+w-r-1, y+r, r, 2, color);
  drawCircleHelper(x+w-r-1, y+h-r-1, r, 4, color);
  drawCircleHelper(x+r, y+h-r-1, r, 8, color);
}

/**************************************************************************/
/*!
   @brief   Draw a rounded rectangle with fill color
    @param    x   Top left corner x coordinate
    @param    y   Top left corner y coordinate
    @param    w   Width in pixels
    @param    h   Height in pixels
    @param    r   Radius of corner rounding
    @param    color 0 - white 1 - black
*/
/**************************************************************************/
void Graphics::fillRoundRect(int16_t x, int16_t y, int16_t w, int16_t h, int16_t r, uint16_t color)
{
  // color = getColor(color);
  // smarter version
  fillRect(x+r, y, w-2*r, h, color);

  // draw four corners
  fillCircleHelper(x+w-r-1, y+r, r, 1, h-2*r-1, color);
  fillCircleHelper(x+r, y+r, r, 2, h-2*r-1, color);
}

/**************************************************************************/
/*!
   @brief   Draw a triangle with no fill color
    @param    x0  Vertex #0 x coordinate
    @param    y0  Vertex #0 y coordinate
    @param    x1  Vertex #1 x coordinate
    @param    y1  Vertex #1 y coordinate
    @param    x2  Vertex #2 x coordinate
    @param    y2  Vertex #2 y coordinate
    @param    color 0 - white 1 - black
*/
/**************************************************************************/
void Graphics::drawTriangle(int16_t x0, int16_t y0,int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color)
{
  // color = getColor(color);
  drawLine(x0, y0, x1, y1, color);
  drawLine(x1, y1, x2, y2, color);
  drawLine(x2, y2, x0, y0, color);
}

/**************************************************************************/
/*!
   @brief     Draw a triangle with color-fill
    @param    x0  Vertex #0 x coordinate
    @param    y0  Vertex #0 y coordinate
    @param    x1  Vertex #1 x coordinate
    @param    y1  Vertex #1 y coordinate
    @param    x2  Vertex #2 x coordinate
    @param    y2  Vertex #2 y coordinate
    @param    color 0 - white 1 - black
*/
/**************************************************************************/
void Graphics::fillTriangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color)
{
  // color = getColor(color);
  int16_t a, b, y, last;

  // Sort coordinates by Y order (y2 >= y1 >= y0)
  if (y0 > y1)
  {
    _swap_int16_t(y0, y1);
    _swap_int16_t(x0, x1);
  }

  if (y1 > y2)
  {
    _swap_int16_t(y2, y1);
    _swap_int16_t(x2, x1);
  }

  if (y0 > y1)
  {
    _swap_int16_t(y0, y1);
    _swap_int16_t(x0, x1);
  }

  if(y0 == y2)
  {
    // Handle awkward all-on-same-line case as its own thing
    a = b = x0;
    if(x1 < a)      a = x1;
    else if(x1 > b) b = x1;
    if(x2 < a)      a = x2;
    else if(x2 > b) b = x2;
    drawFastHLine(a, y0, b-a+1, color);
    return;
  }

  int16_t
  dx01 = x1 - x0,
  dy01 = y1 - y0,
  dx02 = x2 - x0,
  dy02 = y2 - y0,
  dx12 = x2 - x1,
  dy12 = y2 - y1;
  int32_t
  sa   = 0,
  sb   = 0;


  if(y1 == y2) last = y1;   // Include y1 scanline
  else         last = y1-1; // Skip it

  for(y=y0; y<=last; y++)
  {
    a   = x0 + sa / dy01;
    b   = x0 + sb / dy02;
    sa += dx01;
    sb += dx02;

    if(a > b) _swap_int16_t(a,b);
    drawFastHLine(a, y, b-a+1, color);
  }

  // For lower part of triangle, find scanline crossings for segments
  // 0-2 and 1-2.  This loop is skipped if y1=y2.
  sa = dx12 * (y - y1);
  sb = dx02 * (y - y0);

  for(; y<=y2; y++)
  {
    a   = x1 + sa / dy12;
    b   = x0 + sb / dy02;
    sa += dx12;
    sb += dx02;
    /* longhand:
    a = x1 + (x2 - x1) * (y - y1) / (y2 - y1);
    b = x0 + (x2 - x0) * (y - y0) / (y2 - y0);
    */
    if(a > b) _swap_int16_t(a,b);
    drawFastHLine(a, y, b-a+1, color);
  }
}

// BITMAP / XBITMAP / GRAYSCALE / RGB BITMAP FUNCTIONS ---------------------

/**************************************************************************/
/*!
   @brief      Draw a PROGMEM-resident 1-bit image at the specified (x,y) position, using the specified foreground color (unset bits are transparent).
    @param    x   Top left corner x coordinate
    @param    y   Top left corner y coordinate
    @param    bitmap  byte array with monochrome bitmap
    @param    w   Width of bitmap in pixels
    @param    h   Hieght of bitmap in pixels
    @param    color 0 - white 1 - black
*/
/**************************************************************************/
void Graphics::drawBitmapImage(int16_t x, int16_t y, const uint8_t *bitmap, int16_t w, int16_t h, uint16_t color)
{
  // color = getColor(color);
  int16_t i, j, byteWidth = (w + 7) / 8;	// Bitmap scanline pad = whole byte
  uint8_t byte = 0;

  for(j=0; j<h; j++)
  {
    for(i=0; i<w; i++)
    {
      if(i & 7)
    	  byte <<= 1;
      else
    	  byte  = pgm_read_byte(bitmap + j * byteWidth + i / 8);

      if(byte & 0x80)
      {
        drawPixel(x+i, y+j, color);
      }
    }
  }
}

/**************************************************************************/
/*!
    @brief  Set text cursor location
    @param  x    X coordinate in pixels
    @param  y    Y coordinate in pixels
*/
/**************************************************************************/
void Graphics::setCursor(int16_t x, int16_t y)
{
  m_cursorX = x;
  m_cursorY = y;
}

/**************************************************************************/
/*!
    @brief  Get text cursor X location
    @returns    X coordinate in pixels
*/
/**************************************************************************/
int16_t Graphics::getCursorX(void)
{
  return m_cursorX;
}

/**************************************************************************/
/*!
    @brief      Get text cursor Y location
    @returns    Y coordinate in pixels
*/
/**************************************************************************/
int16_t Graphics::getCursorY(void)
{
  return m_cursorY;
}

/**************************************************************************/
/*!
    @brief   Set text 'magnification' size. Each increase in s makes 1 pixel that much bigger.
    @param  s  Desired text size. 1 is default 6x8, 2 is 12x16, 3 is 18x24, etc
*/
/**************************************************************************/
void Graphics::setTextSize(uint8_t s)
{
  m_textSize = (s > 0) ? s : 1;
}

/**************************************************************************/
/*!
    @brief   Set text font color with transparant background
    @param   c   16-bit 5-6-5 Color to draw text with
*/
/**************************************************************************/
void Graphics::setTextColor(uint16_t c)
{
  // For 'transparent' background, we'll set the bg
  // to the same as fg instead of using a flag
  m_textColor = m_textBgColor = c;
}

/**************************************************************************/
/*!
    @brief      Whether text that is too long should 'wrap' around to the next line.
    @param  w Set true for wrapping, false for clipping
*/
/**************************************************************************/
void Graphics::setTextColorWithBG(uint16_t c, uint16_t b)
{
  m_textColor   = c;
  m_textBgColor = b;
}

/**************************************************************************/
/*!
    @brief      Whether text that is too long should 'wrap' around to the next line.
    @param  w Set true for wrapping, false for clipping
*/
/**************************************************************************/
void Graphics::setTextWrap(bool w)
{
  m_wrap = w;
}

/**************************************************************************/
/*!
    @brief
    @param
*/
/**************************************************************************/
uint16_t Graphics::getColor(uint16_t color)
{
  uint16_t tempColor;
  if(m_invertDisplayFlag)
  {
    if(color)
      tempColor = 0 ;
    else
      tempColor = 1 ;
  }
  else
  {
    tempColor = color;
  }
  return tempColor;
}

/**************************************************************************/
/*!
    @brief      Get width of the display, accounting for the current rotation
    @returns    Width in pixels
*/
/**************************************************************************/
int16_t Graphics::width(void)
{
  return m_widthSize;
}

/**************************************************************************/
/*!
    @brief      Get height of the display, accounting for the current rotation
    @returns    Height in pixels
*/
/**************************************************************************/
int16_t Graphics::height(void)
{
  return m_heightSize;
}

/**************************************************************************/
/*!
    @brief      Invert the display (ideally using built-in hardware command)
    @param   i  True if you want to invert, false to make 'normal'
*/
/**************************************************************************/
void Graphics::setInvertDisplay(bool i)
{
  // Do nothing, must be subclassed if supported by hardware
  m_invertDisplayFlag = i;
}

/**************************************************************************/
/*!
    @brief      returns the Invert resule (ideally using built-in hardware command)
    @param   i  True if you want to invert, false to make 'normal'
*/
/**************************************************************************/
bool Graphics::getInvertDisplay()
{
  return m_invertDisplayFlag;
}

/**************************************************************************/
/*!
    @brief      clear or set the image buffer based upon the value of m_invertDisplayFlag
    @param      if m_invertDisplayFlag is true it sets the array buffers to 0xFF;
                if false it sets the array buffers to 0x00
*/
/**************************************************************************/
void Graphics::clearRect(void)
{
  // m_sharpMem.fillRect(0,0,128,128,WHITE);

  if(getInvertDisplay())
  {
    setBuffer();
  }
  else
  {
    clearBuffer();
  }
}

/**************************************************************************/
/*!
    @brief      clear the image buffer sets to 0xFF
    @param      None
*/
/**************************************************************************/
void Graphics::clearBuffer()
{
  memset(m_displayBuffer, 0xff, BUFFER_SIZE);
}

/**************************************************************************/
/*!
    @brief      sets the image buffer sets to 0x00
    @param      None
*/
/**************************************************************************/
void Graphics::setBuffer()
{
  memset(m_displayBuffer, 0x00, BUFFER_SIZE);
}

/**************************************************************************/
/*!
    @brief      returns the pointer to the image buffer
    @param      index if the location in the image buffer.
*/
/**************************************************************************/
uint8_t*  Graphics::getDisplayPtr(uint16_t index)
{
	if ((index < 0) || (index > BUFFER_SIZE))
		return NULL;

	return &m_displayBuffer[index];
}

/**************************************************************************/
/*!
    @brief      OBJECT OF THE GRAPHICS DISPLAY
    @param      None
*/
/**************************************************************************/
Graphics g_Graphics = Graphics();
