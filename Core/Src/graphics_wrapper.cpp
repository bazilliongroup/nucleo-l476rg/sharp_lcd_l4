/**
  ******************************************************************************
  * @file         graphics_wrapper.cpp
  * @brief        Wrapper for C++ class definition.
  *
  *
  ******************************************************************************
  *
  * Copyright (c) 2019-2020 OTTO Engineering, Inc.
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without
  * modification, are prohibited.
  *
  ******************************************************************************
  */

#include "graphics.h"
#include "stdbool.h"

#define GRAPHIC_DEBUG

#ifdef GRAPHIC_DEBUG
extern "C" void Graphics_testGraphicFunction(void);
#endif

//clear functions
extern "C" void Graphics_clearRect(void);
extern "C" void Graphics_clearBuffer(void);

//set functions
extern "C" void Graphics_setInvertDisplay(bool index);
extern "C" void Graphics_setBuffer(void);
extern "C" void Graphics_setCursor(int16_t x, int16_t y);
extern "C" void Graphics_setTextSize(uint8_t s);
extern "C" void Graphics_setTextColor(uint16_t c);
extern "C" void Graphics_setTextColorWithBG(uint16_t c, uint16_t b);
extern "C" void Graphics_setTextWrap(bool w);
extern "C" void Graphics_setFont(uint8_t *bitmap, uint8_t first, uint8_t last, uint8_t yAdvance, 	//GFX_FONT - Data stored for FONT AS A WHOLE: (a GFX_GLYPH* is in the GFX_FONT
	                             uint16_t bitmapOffset, uint8_t  width, uint8_t  height, uint8_t xAdvance, int8_t   xOffset, int8_t   yOffset);	//GFX_GLYPH - Data stored PER GLYPH
extern "C" void Graphics_setDisplayPtr(uint16_t index, uint8_t data);

//get functions
extern "C" uint8_t Graphics_getFontXAdvance(void);
extern "C" uint8_t Graphics_getFontYAdvance(void);
extern "C" uint16_t Graphics_getStringLength(char * str);
extern "C" uint16_t Graphics_getColor(uint16_t color);
extern "C" int16_t Graphics_getCursorX(void);
extern "C" int16_t Graphics_getCursorY(void);
extern "C" int16_t Graphics_getWidth(void);
extern "C" int16_t Graphics_getHeight(void);
extern "C" bool Graphics_getInvertDisplay(void);

//draw functions
extern "C" void Graphics_drawBitmap(int16_t x, int16_t y, const uint8_t *bitmap, int16_t w, int16_t h, uint16_t color);
extern "C" void Graphics_drawPixel(int16_t x, int16_t y, uint16_t color);
extern "C" void Graphics_drawChar(int16_t x, int16_t y, unsigned char c, uint16_t color, uint16_t bg, uint8_t size);
extern "C" void Graphics_drawCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color);
extern "C" void Graphics_drawCircleHelper( int16_t x0, int16_t y0, int16_t r, uint8_t cornername, uint16_t color);
extern "C" void Graphics_drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color);
extern "C" void Graphics_drawRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
extern "C" void Graphics_drawFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color);
extern "C" void Graphics_drawFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color);
extern "C" void Graphics_drawRoundRect(int16_t x, int16_t y, int16_t w, int16_t h, int16_t r, uint16_t color);
extern "C" void Graphics_drawTriangle(int16_t x0, int16_t y0,int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color);
extern "C" void Graphics_drawBitmapImage(int16_t x, int16_t y, const uint8_t *bitmap, int16_t w, int16_t h, uint16_t color);

//fill functions
extern "C" void Graphics_fillCircle(int16_t x0, int16_t y0, int16_t r,uint16_t color);
extern "C" void Graphics_fillCircleHelper(int16_t x0, int16_t y0, int16_t r, uint8_t cornername, int16_t delta, uint16_t color);
extern "C" void Graphics_fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
extern "C" void Graphics_fillScreen(uint16_t color);
extern "C" void Graphics_fillRoundRect(int16_t x, int16_t y, int16_t w, int16_t h, int16_t r, uint16_t color);
extern "C" void Graphics_fillTriangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color);

//write function
extern "C" void Graphics_write(uint8_t c);

//***** typedefs defines *****//

//***** typedefs enums *****//

//***** typedefs structs *****//

//***** API Functions definition *****//
//****Remember "API" is used like "public" in C++*******//

void Graphics_setInvertDisplay(bool index)
{
	g_Graphics.setInvertDisplay(index);
}

void Graphics_clearRect(void)
{
	g_Graphics.clearRect();
}

void Graphics_setBuffer(void)
{
	g_Graphics.setBuffer();
}

void Graphics_clearBuffer(void)
{
	g_Graphics.clearBuffer();
}

void Graphics_setFont(uint8_t *bitmap, uint8_t first, uint8_t last, uint8_t yAdvance, 	//GFX_FONT - Data stored for FONT AS A WHOLE: (a GFX_GLYPH* is in the GFX_FONT
		              uint16_t bitmapOffset, uint8_t  width, uint8_t  height, uint8_t xAdvance, int8_t   xOffset, int8_t   yOffset)	//GFX_GLYPH - Data stored PER GLYPH
{
    Graphics::GFX_GLYPH glyph;       	// Glyph array
    glyph.bitmapOffset = bitmapOffset;
    glyph.width = width;
    glyph.height = height;
    glyph.xAdvance = xAdvance;
    glyph.xOffset = xOffset;
    glyph.yOffset = yOffset;

	Graphics::GFX_FONT font;
	font.bitmap = bitmap; // Glyph bitmaps, concatenated
	font.first = first;
	font.glyph = &glyph;
	font.last = last;
	font.yAdvance = yAdvance;

	g_Graphics.setFont(&font);
}

void Graphics_drawBitmap(int16_t x, int16_t y, const uint8_t *bitmap, int16_t w, int16_t h, uint16_t color)
{
	g_Graphics.drawBitmap(x, y, bitmap, w, h, color);
}

void Graphics_drawPixel(int16_t x, int16_t y, uint16_t color)
{
	g_Graphics.drawPixel(x, y, color);
}

void Graphics_write(uint8_t c)
{
	g_Graphics.write(c);
}

void Graphics_drawChar(int16_t x, int16_t y, unsigned char c, uint16_t color, uint16_t bg, uint8_t size)
{
	g_Graphics.drawChar(x, y, c, color, bg, size);
}

uint16_t Graphics_getStringLength(char * str)
{
	return g_Graphics.getStringLength(str);
}

uint8_t Graphics_getFontXAdvance(void)
{
	return g_Graphics.getFontXAdvance();
}

uint8_t Graphics_getFontYAdvance(void)
{
	return g_Graphics.getFontYAdvance();
}

void Graphics_drawCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color)
{
	g_Graphics.drawCircle(x0, y0, r, color);
}

void Graphics_drawCircleHelper( int16_t x0, int16_t y0, int16_t r, uint8_t cornername, uint16_t color)
{
	g_Graphics.drawCircleHelper(x0, y0, r, cornername, color);
}

void Graphics_fillCircle(int16_t x0, int16_t y0, int16_t r,uint16_t color)
{
	g_Graphics.fillCircle(x0, y0, r, color);
}

void Graphics_fillCircleHelper(int16_t x0, int16_t y0, int16_t r, uint8_t cornername, int16_t delta, uint16_t color)
{
	g_Graphics.fillCircleHelper(x0, y0, r, cornername, delta, color);
}

void Graphics_drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color)
{
	g_Graphics.drawLine(x0, y0, x1, y1, color);
}

void Graphics_drawRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color)
{
	g_Graphics.drawRect(x, y, w, h, color);
}

void Graphics_drawFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color)
{
	g_Graphics.drawFastVLine(x, y, h, color);
}

void Graphics_drawFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color)
{
	g_Graphics.drawFastHLine(x, y, w, color);
}

void Graphics_fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color)
{
	g_Graphics.fillRect(x, y, w, h, color);
}

void Graphics_fillScreen(uint16_t color)
{
	g_Graphics.fillScreen(color);
}

void Graphics_drawRoundRect(int16_t x, int16_t y, int16_t w, int16_t h, int16_t r, uint16_t color)
{
	g_Graphics.drawRoundRect(x, y, w, h, r, color);
}

void Graphics_fillRoundRect(int16_t x, int16_t y, int16_t w, int16_t h, int16_t r, uint16_t color)
{
	g_Graphics.fillRoundRect(x, y, w, h, r, color);
}

void Graphics_drawTriangle(int16_t x0, int16_t y0,int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color)
{
	g_Graphics.drawTriangle(x0, y0, x1, y1, x2, y2, color);
}

void Graphics_fillTriangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color)
{
	g_Graphics.fillTriangle(x0, y0, x1, y1, x2, y2, color);
}

void Graphics_drawBitmapImage(int16_t x, int16_t y, const uint8_t *bitmap, int16_t w, int16_t h, uint16_t color)
{
	g_Graphics.drawBitmapImage(x, y, bitmap, w, h, color);
}

void Graphics_setCursor(int16_t x, int16_t y)
{
	g_Graphics.setCursor(x, y);
}

int16_t Graphics_getCursorX(void)
{
	return g_Graphics.getCursorX();
}

int16_t Graphics_getCursorY(void)
{
	return g_Graphics.getCursorY();
}

void Graphics_setTextSize(uint8_t s)
{
	g_Graphics.setTextSize(s);
}

void Graphics_setTextColor(uint16_t c)
{
	g_Graphics.setTextColor(c);
}

void Graphics_setTextColorWithBG(uint16_t c, uint16_t b)
{
	g_Graphics.setTextColorWithBG(c, b);
}

void Graphics_setTextWrap(bool w)
{
	g_Graphics.setTextWrap(w);
}

uint16_t Graphics_getColor(uint16_t color)
{
	return g_Graphics.getColor(color);
}

int16_t Graphics_getWidth(void)
{
	return g_Graphics.width();
}

int16_t Graphics_getHeight(void)
{
	return g_Graphics.height();
}

bool Graphics_getInvertDisplay(void)
{
	return g_Graphics.getInvertDisplay();
}

void Graphics_setDisplayPtr(uint16_t index, uint8_t data)
{
	g_Graphics.setDisplayPtr(index, data);
}

#ifdef GRAPHIC_DEBUG

void Graphics_testGraphicFunction(void)
{
//	g_Graphics.clearRect();

//	uint16_t res;
//	int j = 8;
//	char str[] = {'a'};
//	res = g_Graphics.getStringLength(&str[0]);
//	for (int i = 0; i < (int)res; i++)
//	{
//		g_Graphics.drawChar(115, 150, 0x50, 0, 0, 3);
//	}

}

#endif
