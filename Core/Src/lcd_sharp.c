/*
Name:				sharp_lcd.c
Library:			LCD Driver for STM32 MCUs
Written by:			Ronald Bazillion
Date written:		02/13/2020
Description:		This is the lcd implementation file for the sharp LS018B7DH02

                    The Sharp display sends the data in LSB Least Significant Bit format.

Resources:

Modifications:
    02/13/2020      Ronald Bazillion        First Draft

-----------------
*/

//***** Header files *****//
#include <lcd_graphics_prototypes.h>
#include <string.h>
#include "stdbool.h"
#include "lcd_sharp.h"
#include "images.inc"
//***** typedefs enums *****//


//***** typedefs structs *****//


//***** static Library variables *****//
//****Remember "static" is used like "private" in C++*******//

static LCD_SHARP_CONFIG m_sharpConfig;
static bool m_vCom;				//Vcom State
static bool m_displayMutex;		//Mutex used to keep Vcom from sending while writing data
static bool m_vComDelayed;		//Signal used to signal vcom eeds to be fired after recource is released

static uint8_t m_lineFrameBuffer[NUM_OF_PIXEL_BYTES];
static uint8_t m_commandBuffer[NUM_OF_COMMAND_BYTES];

static uint8_t m_frmBuffer[DISPLAY_LENGTH_SIZE][NUM_OF_PIXEL_BYTES];

//An array to flip the 4 bit nibbles from MSB to LSB
static uint8_t m_bflipnib[] =
{
		0x0, 0x8, 0x4, 0xc,
		0x2, 0xa, 0x6, 0xe,
		0x1, 0x9, 0x5, 0xd,
		0x3, 0xb, 0x7, 0xf
};

//*********static function prototypes***********************//
//****Remember "static" is used like "private" in C++*******//

static void m_sharpSendData(uint16_t start, uint16_t end);
static void m_writeLine(uint8_t* buffer, uint16_t size);
static void m_sendVcom(void);
static void m_clearBuffer(void);
static void m_clearScreen(void);
static void m_initScreen(int color);
static void m_mvGraphicsDisplayToBuffer(void);

#ifdef FUTURE_DEV
static void m_toggleVcom(uint8_t* commandByte);
#endif

//***** API Functions definition *****//
//****Remember "API" is used like "public" in C++*******//

void LCD_Sharp_Init(LCD_SHARP_CONFIG* lcdConfig)
{
	m_sharpConfig.extMode = lcdConfig->extMode;
	m_sharpConfig.gpioCsPin = lcdConfig->gpioCsPin;
	m_sharpConfig.gpioCsPort = lcdConfig->gpioCsPort;
	m_sharpConfig.gpioDispPin = lcdConfig->gpioDispPin;
	m_sharpConfig.gpioDispPort = lcdConfig->gpioDispPort;
	m_sharpConfig.gpioExtComInPin = lcdConfig->gpioExtComInPin;
	m_sharpConfig.gpioExtComInPort = lcdConfig->gpioExtComInPort;
	m_sharpConfig.gpioExtModePin = lcdConfig->gpioExtModePin;
	m_sharpConfig.gpioExtModePort = lcdConfig->gpioExtModePort;
	m_sharpConfig.hspi = lcdConfig->hspi;

	m_vCom = false;
	m_displayMutex = false;
	m_vComDelayed = false;
	memset(&m_lineFrameBuffer[0], 0x00, NUM_OF_PIXEL_BYTES);

	LynqGraphics_initDisplayLibrary();

	//Set Power on Sequence
	HAL_GPIO_WritePin(m_sharpConfig.gpioExtComInPort, m_sharpConfig.gpioExtComInPin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(m_sharpConfig.gpioCsPort, m_sharpConfig.gpioCsPin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(m_sharpConfig.gpioDispPort, m_sharpConfig.gpioDispPin, GPIO_PIN_RESET);
	HAL_Delay(10);
	memset(&m_commandBuffer[0], 0x00, NUM_OF_COMMAND_BYTES);
	m_commandBuffer[0] |= (uint8_t)LCD_CLR_BR;
	m_writeLine(&m_commandBuffer[0], NUM_OF_COMMAND_BYTES);
	HAL_Delay(1);
	m_writeLine(&m_commandBuffer[0], NUM_OF_COMMAND_BYTES);
	HAL_Delay(1);
	HAL_GPIO_WritePin(m_sharpConfig.gpioDispPort, m_sharpConfig.gpioDispPin, GPIO_PIN_SET);
}

void LCD_Sharp_InitScreen(int color)
{
	m_initScreen(color);
}

void LCD_Sharp_ClearScreen(void)
{
	m_clearScreen();
}

void LCD_Sharp_Invalidate(uint16_t startLength, uint16_t endLength, uint16_t startPixel, uint16_t endPixel, bool clearSel)
{
    //protections
    if (endLength > DISPLAY_LENGTH_SIZE){ endLength = DISPLAY_LENGTH_SIZE; }
    if (startLength > endLength) { startLength = 0; }
    if (endPixel >= (NUM_OF_PIXEL_BYTES-2)) { endPixel = ((NUM_OF_PIXEL_BYTES-2)-1); }
    if (startPixel > endPixel) { startPixel = 0; }
    if (startPixel < 2) { startPixel = 2; }

    for (uint16_t w = startLength; w < endLength; w++)
    {
        //Each Command Address is 16 bytes long and bit reversed.
        //[M0][M1][M2][DUM][DUM][DUM][DUM][AG0][AG1][AG2][AG3][AG4][AG5][AG6][AG7][AG8]
        m_frmBuffer[w][0] = ((w+1) & LCD_WR_BR) << 7 | 0x01;
        m_frmBuffer[w][1] = ((w+1) >> 1 ) & 0xFF;

        //Add the data into the frame buffers
        for (uint16_t l = startPixel; l < endPixel; l++)
        {
            //Each bit represents one pixel in a byte so each byte has 8 pixels.
            if (clearSel)
            {
                m_frmBuffer[w][l] = 0xff;
            }
            else
            {
                m_frmBuffer[w][l] = 0x00;
            }
        }

        //Add in the trailer bits
        m_frmBuffer[w][NUM_OF_PIXEL_BYTES-2] = 0;
        m_frmBuffer[w][NUM_OF_PIXEL_BYTES-1] = 0;
    }

    //Send the Update
    for (uint16_t l = startLength; l < endLength; l++)
    {
        m_writeLine(&m_frmBuffer[l][0], NUM_OF_PIXEL_BYTES);
    }
}

void LCD_Sharp_DisplayImage(uint16_t edWidth, uint16_t edLen, int color, const uint8_t* inGraphic)
{

	//Protection
	if ((edWidth == 0) || (edLen == 0) || inGraphic == NULL) { return; }

	m_clearBuffer();
	m_initScreen(color);

	//scroll through the width of the graphic 0 --> 200 bits
	for (uint16_t i = 0; i < edWidth; i++)
	{
		//Each Command Address is 16 bytes long and bit reversed.
		//[M0][M1][M2][DUM][DUM][DUM][DUM][AG0][AG1][AG2][AG3][AG4][AG5][AG6][AG7][AG8]
		m_frmBuffer[i][0] = ((i+1) & LCD_WR_BR) << 7 | 0x01;
		m_frmBuffer[i][1] = ((i+1) >> 1 ) & 0xFF;

		//put in the data 25 bytes = 200 bits
		for (uint16_t j = 2; j < edLen; j++)
		{
			//Each frame is in multiples of edLen bytes
			uint8_t bflp = inGraphic[edLen*i+(j-2)];

			//We have to flip the bits and put into buffer
			m_frmBuffer[i][j+2] = m_bflipnib[(bflp>>4)&0xf]|(m_bflipnib[(bflp&0xf)]<<4);
		}

		//Add in the trailer bits
		m_frmBuffer[i][NUM_OF_PIXEL_BYTES-2] = 0;
		m_frmBuffer[i][NUM_OF_PIXEL_BYTES-1] = 0;
	}

	for (uint16_t i = 0; i < 303; i++)
	{
		m_writeLine(&m_frmBuffer[i][0], NUM_OF_PIXEL_BYTES);
	}
}

void LCD_Sharp_WritePartialUpdate(uint16_t startLength,	uint16_t endLength, uint16_t startPixel, uint16_t endPixel, const uint8_t* inGraphic)
{
    //protections
    if ((endLength > DISPLAY_LENGTH_SIZE) ||
            (startLength > endLength) ||
            (endPixel >= (NUM_OF_PIXEL_BYTES-2)) ||
            (startPixel > endPixel) ||
            (startPixel < 2))
    {
        return;
    }

    for (uint16_t w = startLength; w < endLength; w++)
    {
        //Each Command Address is 16 bytes long and bit reversed.
        //[M0][M1][M2][DUM][DUM][DUM][DUM][AG0][AG1][AG2][AG3][AG4][AG5][AG6][AG7][AG8]
        m_frmBuffer[w][0] = ((w+1) & LCD_WR_BR) << 7 | 0x01;
        m_frmBuffer[w][1] = ((w+1) >> 1 ) & 0xFF;

        uint8_t lenDifference = endPixel - startPixel;
        uint8_t graphicStart = w - startLength;

        //Add the data into the frame buffers
        for (uint16_t l = startPixel; l < endPixel; l++)
        {
            //Each bit represents one pixel in a byte so each byte has 8 pixels.
            //We have to bit flip the data
            volatile uint8_t bflp = inGraphic[lenDifference*graphicStart+(l-startPixel)];
            m_frmBuffer[w][l] = m_bflipnib[(bflp >> 4 & 0xF)] | (m_bflipnib[(bflp & 0xF)] << 4);
        }

        //Add in the trailer bits
        m_frmBuffer[w][NUM_OF_PIXEL_BYTES-2] = 0;
        m_frmBuffer[w][NUM_OF_PIXEL_BYTES-1] = 0;
    }

    //Send the Update
    for (uint16_t l = startLength; l < endLength; l++)
    {
        m_writeLine(&m_frmBuffer[l][0], NUM_OF_PIXEL_BYTES);
    }
}

void LCD_Sharp_PowerOffSequence(void)
{
    //Set Power off Sequence
    memset(&m_commandBuffer[0], 0x00, NUM_OF_COMMAND_BYTES);
    m_commandBuffer[0] |= (uint8_t)LCD_CLR_BR;
    m_writeLine(&m_commandBuffer[0], NUM_OF_COMMAND_BYTES);
    HAL_Delay(1);
    m_writeLine(&m_commandBuffer[0], NUM_OF_COMMAND_BYTES);
    HAL_Delay(1);
    HAL_GPIO_WritePin(m_sharpConfig.gpioDispPort, m_sharpConfig.gpioDispPin, GPIO_PIN_RESET);
}

void LCD_Sharp_SendVcom(void)
{
	m_sendVcom();
}

void LCD_Sharp_SendData(void)
{
	for (uint16_t i = 0; i < 303; i++)
	{
		m_writeLine(&m_frmBuffer[i][0], NUM_OF_PIXEL_BYTES);
	}

//	m_sharpSendData(0, DISPLAY_LENGTH_SIZE);
}

void LCD_Sharp_MvGraphicsDisplayToBuffer(void)
{
	m_mvGraphicsDisplayToBuffer();
}

void LCD_Sharp_GraphicsTest(void)
{
		m_initScreen(WHITE);
		HAL_Delay(1000);
		Graphics_setInvertDisplay(true);	//true is bg is blacks fg is white and false bg is white fg is black
		Graphics_clearRect();

//		LynqGraphics_drawDisplayLynqLabel(0);	//0 is white, 1 is black;

		Graphics_setCursor(20, 120);		//Set text to center of circle.
		uint8_t myBuffer[14];
		memcpy(&myBuffer[0], "Rons Graphics\n", 14);
		LynqGraphics_writeStream(&myBuffer[0], 14);

//		for (uint16_t i = 0; i < 20; i++)
//		{
//			LynqGraphics_drawShapePoint(0, 100+i, 100+i);
//			LynqGraphics_drawShapeLine(false, 0, 100+i, 100, 100+i);
//		}

		LynqGraphics_drawShapeCircle(0, 115, 111, 110);
		Graphics_fillCircle(140, 160, 20, 0);
		LynqGraphics_drawShapeTriangle(0, 70, 70, 120, 20, 170, 70);
//		LynqGraphics_drawShapeRectangle(0, 50, 50, 50, 100);
//		LynqGraphics_drawShapeFillRect(0, 50, 50, 50, 100);
		LynqGraphics_drawShapeFillRoundRect(0, 55, 140, 50, 50);
//		LynqGraphics_drawShapeArc(0, 50, 115, 200);
//		LynqGraphics_drawShapeArrow(0, 90);
		LynqGraphics_drawShapeBoldArc(0, 115, 120, 110, 180 % 360, 65 % 360);

//		uint16_t angle = 90;/* In degrees */
//		uint16_t arcLength = 30;/* In degrees */
//		char name[] = "Alpha";
//
//		uint16_t nameLen = 5;/* Max Lenght 6. Refer MAXNAMELEN */
//		char name2[] = "Beta";
//
//		uint16_t nameLen2 = 4;/* Max Lenght 6. Refer MAXNAMELEN */
//		uint16_t distance = 123; /* Max length 4, Refer MAXDISTANCECHARS */
//
//		Graphics_setBuffer();
//		LynqGraphics_drawMemberScreen(name2, nameLen2, angle, arcLength, distance);/* Draws Device Team Member Screen displayBuffer */

		//Reset Offsets
		LynqGraphics_setCursorOffset(0, 0);
		Graphics_setTextSize(1);
		LynqGraphics_setLineNum(0);

		m_mvGraphicsDisplayToBuffer();
		m_sharpSendData(0, DISPLAY_LENGTH_SIZE);
}

void LCD_Sharp_InterruptCallBack_ISR(uint8_t var)
{
	if (!m_displayMutex)		//if recourse is not being used
	{
//		m_sendVcom();
	}
	else
	{
//		m_vComDelayed = true;	//set vcom delayed because spi is sending data.
	}
}

//************************************************** STATIC FUNCTIONS ***********************************************
//*******************************************************************************************************************
//*******************************************************************************************************************

static void m_sharpSendData(uint16_t start, uint16_t end)
{
	if (start < 0 || end > DISPLAY_LENGTH_SIZE)
		return;

    for (uint16_t i = start; i < end; i++)
    {
        HAL_GPIO_WritePin(m_sharpConfig.gpioCsPort, m_sharpConfig.gpioCsPin, GPIO_PIN_SET);
        HAL_SPI_Transmit(&m_sharpConfig.hspi, m_frmBuffer[i], NUM_OF_PIXEL_BYTES, 10);
        HAL_GPIO_WritePin(m_sharpConfig.gpioCsPort, m_sharpConfig.gpioCsPin, GPIO_PIN_RESET);
    }
}

static void m_writeLine(uint8_t* buffer, uint16_t size)
{
	m_displayMutex = true;
	HAL_GPIO_WritePin(m_sharpConfig.gpioCsPort, m_sharpConfig.gpioCsPin, GPIO_PIN_SET);
	HAL_SPI_Transmit(&m_sharpConfig.hspi, buffer, size, 10);
	HAL_GPIO_WritePin(m_sharpConfig.gpioCsPort, m_sharpConfig.gpioCsPin, GPIO_PIN_RESET);
	m_displayMutex = false;

	//if Vcom signal came in during a write.
	if (m_vComDelayed)
	{
		m_sendVcom();				//Send VCom signal
		m_vComDelayed = false;		//Reset signal
	}
}

static void m_mvGraphicsDisplayToBuffer(void)
{
    for ( uint16_t i = 0; i < DISPLAY_LENGTH_SIZE; i++)
    {
        //Each Command Address is 16 bytes long and bit reversed.
        //[M0][M1][M2][DUM][DUM][DUM][DUM][AG0][AG1][AG2][AG3][AG4][AG5][AG6][AG7][AG8]
        m_frmBuffer[i][0] = ((i+1) & LCD_WR_BR) << 7 | 0x01;
        m_frmBuffer[i][1] = ((i+1) >> 1 ) & 0xFF;

        for ( uint16_t j = 2; j < (NUM_OF_PIXEL_BYTES-2); j++ )
        {
            m_frmBuffer[i][j] = *(LynqGraphics_getDisplayPtr(i*(NUM_OF_PIXEL_BYTES-4)+(j-2)));
        }

        //Add in the trailer bits
		m_frmBuffer[i][NUM_OF_PIXEL_BYTES-2] = 0;
		m_frmBuffer[i][NUM_OF_PIXEL_BYTES-1] = 0;
    }
}

#ifdef FUTURE_DEV
//NOTE: builds the line frame.
//
static void m_toggleVcom(uint8_t* commandByte)
{
	if (m_vCom)	//V = 1
	{
		m_vCom = false;
		*commandByte &= ~(0x40);	//set V to 0
	}
	else	//V = 0
	{
		m_vCom = true;
		*commandByte |= 0x40;		//set V to 1
	}
}
#endif

static void m_sendVcom(void)
{
	memset(&m_commandBuffer[0], 0x00, NUM_OF_COMMAND_BYTES);
	m_commandBuffer[0] = (uint8_t)LCD_WR_BR | LCD_VCOM_BR;		//if VCOM bit is 0 we want to send a 0 so we will enable the write bit.
	m_writeLine(&m_commandBuffer[0], NUM_OF_COMMAND_BYTES);
}

static void m_clearBuffer(void)
{
    //clear buffer
    for ( uint16_t i = 0; i < DISPLAY_LENGTH_SIZE; i++)
    {
        for ( uint16_t j = 2; j < (NUM_OF_PIXEL_BYTES-2); j++ )
        {
            m_frmBuffer[i][j] = 0;
        }
    }
}

static void m_clearScreen(void)
{
    memset(&m_commandBuffer[0], 0x00, NUM_OF_COMMAND_BYTES);
    m_commandBuffer[0] |= (uint8_t)LCD_CLR_BR;
    m_writeLine(&m_commandBuffer[0], NUM_OF_COMMAND_BYTES);
}

static void m_initScreen(int color)
{
    for (uint16_t w = 0; w < DISPLAY_LENGTH_SIZE; w++)
    {
        //Each Command Address is 16 bytes long and bit reversed.
        //[M0][M1][M2][DUM][DUM][DUM][DUM][AG0][AG1][AG2][AG3][AG4][AG5][AG6][AG7][AG8]
        m_frmBuffer[w][0] = ((w+1) & LCD_WR_BR) << 7 | 0x01;
        m_frmBuffer[w][1] = ((w+1) >> 1 ) & 0xFF;

        //Add the data into the frame buffers
        for (uint16_t l = 2; l < (NUM_OF_PIXEL_BYTES-2); l++)
        {
            if (color == BLACK)
            {
                m_frmBuffer[w][l] = 0x00;
            }
            else if (color == WHITE)
            {
                m_frmBuffer[w][l] = 0xFF;
            }
        }

        //Add in the trailer bits
        m_frmBuffer[w][NUM_OF_PIXEL_BYTES-2] = 0;
        m_frmBuffer[w][NUM_OF_PIXEL_BYTES-1] = 0;
    }

    for (uint16_t w = 0; w < DISPLAY_LENGTH_SIZE; w++)
    {
        m_writeLine(&m_frmBuffer[w][0], NUM_OF_PIXEL_BYTES);
    }
}
