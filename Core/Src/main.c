/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "lcd_sharp.h"
#include "timer_manager.h"
#include "app_gui_gfx.h"
#include "images.inc"
#include "lcd_fonts_24x33.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;
DMA_HandleTypeDef hdma_spi1_tx;

TIM_HandleTypeDef htim6;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_SPI1_Init(void);
static void MX_TIM6_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
uint8_t LcdTmrId = UNSUCCESSFUL;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_SPI1_Init();
  MX_TIM6_Init();
  /* USER CODE BEGIN 2 */
  LCD_SHARP_CONFIG lcdConfig;
  lcdConfig.gpioCsPort = SPI1_CS_GPIO_Port;
  lcdConfig.gpioCsPin = SPI1_CS_Pin;
  lcdConfig.gpioDispPort = DISP_GPIO_Port;
  lcdConfig.gpioDispPin = DISP_Pin;
  lcdConfig.gpioExtComInPort = EXTCOMIN_GPIO_Port;
  lcdConfig.gpioExtComInPin = EXTCOMIN_Pin;
  lcdConfig.gpioExtModePort = EXTMODE_GPIO_Port;
  lcdConfig.gpioExtModePin = EXTMODE_Pin;
  lcdConfig.hspi = hspi1;
  lcdConfig.extMode = 0;

  //Timer Manager Init
  HAL_TIM_Base_Start_IT(&htim6);
  TmrInit(&htim6);

  //LCD
  LCD_Sharp_Init(&lcdConfig);
  LCD_Sharp_InitScreen(BLACK);
  HAL_Delay(3000);
//  LCD_Sharp_Invalidate(100, 200, 10, 20, false);
//  HAL_Delay(3000);
//  LCD_Sharp_Invalidate(100, 200, 10, 20, true);

//For now the VCOM isn't being used. Looks like you don't need it.
  LcdTmrId = TmrCreateTimer(500, LCD_Sharp_InterruptCallBack_ISR, false);
  if (TmrIsEnabled(LcdTmrId))
  {
	  TmrStart(LcdTmrId);
  }
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
		LCD_Sharp_GraphicsTest();
		HAL_Delay(3000);
	    LCD_Sharp_DisplayImage(303, 28, BLACK, &ronb_224_303[0]);
	    HAL_Delay(3000);
	    LCD_Sharp_DisplayImage(303, 28, BLACK, &mgrs_image[0]);
	    HAL_Delay(1000);
	    LCD_Sharp_WritePartialUpdate(115, 148, 10, 13, &two_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(115, 148, 13, 16, &four_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(115, 148, 16, 19, &eight_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(115, 148, 19, 22, &three_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(115, 148, 22, 25, &one_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(251, 285, 20, 23, &five_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(251, 285, 23, 26, &zero_24x33_image[0]);
	    HAL_Delay(3000);
	    LCD_Sharp_WritePartialUpdate(173, 206, 10, 13, &nine_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(173, 206, 13, 16, &one_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(173, 206, 16, 19, &seven_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(173, 206, 19, 22, &six_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(173, 206, 22, 25, &zero_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(251, 285, 20, 23, &two_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(251, 285, 23, 26, &zero_24x33_image[0]);
	    HAL_Delay(3000);

	    LCD_Sharp_Invalidate(0, 40, 23, 31, false);
	    LCD_Sharp_Invalidate(57, 90, 5, 15, false);
	    LCD_Sharp_Invalidate(57, 90, 16, 25, false);
		LCD_Sharp_WritePartialUpdate(0, 40, 2, 23, &long_lat[0]);
		LCD_Sharp_WritePartialUpdate(115, 148, 07, 10, &three_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(115, 148, 10, 13, &three_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(115, 148, 13, 16, &dot_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(115, 148, 16, 19, &eight_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(115, 148, 19, 22, &zero_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(115, 148, 22, 25, &nine_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(115, 148, 25, 28, &n_24x33_image[0]);

	    LCD_Sharp_WritePartialUpdate(173, 206, 07, 10, &one_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(173, 206, 10, 13, &one_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(173, 206, 13, 16, &eight_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(173, 206, 16, 19, &dot_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(173, 206, 19, 22, &zero_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(173, 206, 22, 25, &five_24x33_image[0]);
	    LCD_Sharp_WritePartialUpdate(173, 206, 25, 28, &w_24x33_image[0]);
	    HAL_Delay(3000);
	    LCD_Sharp_WritePartialUpdate(115, 148, 07, 10, &three_24x33_image[0]);
		LCD_Sharp_WritePartialUpdate(115, 148, 10, 13, &three_24x33_image[0]);
		LCD_Sharp_WritePartialUpdate(115, 148, 13, 16, &dot_24x33_image[0]);
		LCD_Sharp_WritePartialUpdate(115, 148, 16, 19, &two_24x33_image[0]);
		LCD_Sharp_WritePartialUpdate(115, 148, 19, 22, &seven_24x33_image[0]);
		LCD_Sharp_WritePartialUpdate(115, 148, 22, 25, &three_24x33_image[0]);
		LCD_Sharp_WritePartialUpdate(115, 148, 25, 28, &n_24x33_image[0]);

		LCD_Sharp_WritePartialUpdate(173, 206, 07, 10, &one_24x33_image[0]);
		LCD_Sharp_WritePartialUpdate(173, 206, 10, 13, &one_24x33_image[0]);
		LCD_Sharp_WritePartialUpdate(173, 206, 13, 16, &eight_24x33_image[0]);
		LCD_Sharp_WritePartialUpdate(173, 206, 16, 19, &dot_24x33_image[0]);
		LCD_Sharp_WritePartialUpdate(173, 206, 19, 22, &four_24x33_image[0]);
		LCD_Sharp_WritePartialUpdate(173, 206, 22, 25, &one_24x33_image[0]);
		LCD_Sharp_WritePartialUpdate(173, 206, 25, 28, &w_24x33_image[0]);
		HAL_Delay(3000);
	  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  HAL_PWR_EnableBkUpAccess();
  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 20;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_4BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 8720;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 10;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Channel4_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Channel4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Channel4_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(SPI1_CS_GPIO_Port, SPI1_CS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, DISP_Pin|EXTCOMIN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(EXTMODE_GPIO_Port, EXTMODE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : SPI1_CS_Pin */
  GPIO_InitStruct.Pin = SPI1_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(SPI1_CS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : DISP_Pin EXTCOMIN_Pin */
  GPIO_InitStruct.Pin = DISP_Pin|EXTCOMIN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : EXTMODE_Pin */
  GPIO_InitStruct.Pin = EXTMODE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(EXTMODE_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
